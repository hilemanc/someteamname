﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystematicManager.Models.Entities
{
    /// <summary>
	/// This class handles all the Protocol related items. It includes
	///     arbitrarily deep nested child Sections.
	/// </summary>
	public class Protocol
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string InclusionCriteria { get; set; }
        public string ExclusionCriteria { get; set; }
        public string Authors { get; set; }
        public List<Section> ChildSections { get; set; }

        
        

        public Protocol(string title)
        {
            this.Title = title;
            this.ChildSections = new List<Section>(0);

            // Itialize the values
            this.InclusionCriteria = "";
            this.ExclusionCriteria = "";

            // Because there's no Project connection and class yet...
            this.Authors = "John Doe, Jane Doe, Santa Claus";
        }


        public Protocol(string title, string authors)
        {
            this.Title = title;
            this.ChildSections = new List<Section>(0);
            this.Authors = authors;

            // Initialize the values
            this.InclusionCriteria = "";
            this.ExclusionCriteria = "";
        }

        /// <summary>
        /// Copy consturctor for the protocol class that performs a deep copy on the sections
        /// </summary>
        /// <param name="protocol">The protocol that is being copied</param>
        public Protocol(Protocol protocol)
        {
            this.Title = protocol.Title;
            this.Authors = protocol.Authors;
            this.ExclusionCriteria = protocol.ExclusionCriteria;
            this.InclusionCriteria = protocol.InclusionCriteria;
            this.ChildSections = protocol.CopyChildSections();
        }

        /// <summary>
        /// Adds a new Section to this object's ChildSections.
        ///     The correct ID to pass to the child Section's constructor
        ///     is calculated here.
        /// </summary>
        /// <returns>Instance of the new Section</returns>
        public Section AddChildSection()
        {
            string nextId = "";
            foreach (Section sec in ChildSections)
                nextId = (int.Parse(sec.Id) + 1).ToString();
            if (nextId == "" || nextId == null)
                nextId = "0";
            Section section = new Section(nextId);
            ChildSections.Add(section);
            return section;
        }

        /// <summary>
        /// Adds a new Section to this object's Child Sections, and
        /// populates newly constructed section with passed information.
        /// </summary>
        /// <param name="heading">Heading of new Section</param>
        /// <param name="text">Heading of text Section</param>
        /// <returns></returns>
        public Section AddChildSection(string heading, string text)
        {
            if (heading == null || text == null)
                throw new ArgumentNullException();
            string nextId = "";
            foreach (Section sec in ChildSections)
                nextId = (int.Parse(sec.Id) + 1).ToString();
            if (nextId == "" || nextId == null)
                nextId = "0";
            Section section = new Section(nextId);
            section.Heading = heading;
            section.Text = text;
            ChildSections.Add(section);
            return section;
        }

        /// <summary>
        /// Copy method that takes a section and adds it the the child section's list
        /// </summary>
        /// <param name="section">The section that is being added to the Protocol</param>
        /// <returns>The section that was passed into it</returns>
        public Section AddChildSection(Section section)
        {
            this.ChildSections.Add(section);

            return section;
        }

        /// <summary>
        /// Updates the Section with the param id.
        /// </summary>
        /// <param name="id">Section id</param>
        /// <param name="text">Text field</param>
        /// <param name="heading">Heading field</param>
        public void UpdateSection(string id, string text, string heading)
        {
            Section sec = GetSection(id);
            sec.Text = text;
            sec.Heading = heading;
        }

        /// <summary>
        /// Deletes the requested Section from the ChildSections of this object.
        ///     Note that this will also make that child's Sections inaccessible.
        /// </summary>
        /// <param name="id"></param>
        public void RemoveSection(string id)
        {
            int index = -1;
            for (int i = 0; i < ChildSections.Count; i++)
                if (ChildSections[i].Id == id)
                    index = i;
            if (index != -1)
                ChildSections.RemoveAt(index);
        }

        /// <summary>
        /// Finds the Section with the requested ID. among all Sections.
        /// </summary>
        /// <param name="id">String ID of the Section</param>
        /// <returns>Section instance</returns>
        public Section GetSection(string id)
        {
            // Iterate over Protocol's Sections, find Section where first id char matches param id
            foreach (Section sec in ChildSections)
                if (sec.Id[0] == id[0])
                    return (sec.GetSection(id));
            throw new Exception("Error: Section could not be found");
        }

        /// <summary>
        /// Finds the FIRST Section with the requested heading among all Sections.
        /// </summary>
        /// <param name="heading">String heading of the Section</param>
        /// <returns>Section instance</returns>
        public Section GetSectionByHeading(string heading)
        {
            // Iterate over Protocol's Sections, find Section where first id char matches param id
            foreach (Section sec in ChildSections)
            {
                if (sec.Heading == heading)
                    return sec;
                else
                {
                    Section temp = sec.GetSectionByHeading(heading);
                    if (temp != null)
                        return temp;
                }
            }
            return null;
        }

        /// <summary>
        /// Returns all child Sections
        /// </summary>
        /// <returns>ChildSections</returns>
        public List<Section> GetChildSections()
        {
            return ChildSections;
        }

        /// <summary>
        /// Generates a formatted string of the Protocol details and all its
        ///     child Sections. Depends on the Section ToString(), which may
        ///     call itself recursively. For performance, this shouldn't be called
        ///     on Protocols with arbitrarily nested Sections.
        /// </summary>
        /// <returns>Formatted string of Protocol details</returns>
        public override string ToString()
        {
            string output = "";
            output += "Protocol: Title = " + Title + "\n";
            output += "\tAuthors = " + Authors + "\n";
            foreach (Section sec in ChildSections)
            {
                output += sec.ToString();
            }
            return output;
        }

        /// <summary>
        /// Converts section tree into linear list.
        /// </summary>
        /// <returns>A List representation of this protocol.</returns>
        public List<Section> ToChildrenList()
        {
            List<Section> list = new List<Section>(50);
            foreach (Section sec in ChildSections)
                sec.AddToList(ref list);
            return list;
        }

        /// <summary>
        /// Generate the string representation of the protocol and its sections that will be used to export to a file
        /// </summary>
        /// <returns>A pipe separated string of Protocol fields and protocol's section's fields. Doesn't have a final separator</returns>
        public List<string> CreateProtocolString()
        {
            List<string> stringsToWrite;    // Get a list of the properties to write to the file

            stringsToWrite = new List<string>();

            //Copy the following fields from the protocol class to the List
            stringsToWrite.Add(this.Title);
            stringsToWrite.Add(this.InclusionCriteria);
            stringsToWrite.Add(this.ExclusionCriteria);
            stringsToWrite.Add(this.Authors);

            List<string> sectionList = CreateSectionStrings();
            for (int i = 0; i < sectionList.Count; i++)
            {
                stringsToWrite.Add(sectionList[i]);
            }


            return stringsToWrite;//MakeString ( stringsToWrite.ToArray ( ), seperator );
        }


        /// <summary>
        /// Combine the string list into a single string for display
        /// </summary>
        /// <param name="strings">The list of strings that are to be appended to a string</param>
        /// <param name="seperator">The seperator that is being used to delimit the file.</param>
        /// <returns>A string that represents the Protocol setup for saving to a file</returns>
        private string MakeString(string[] strings, char seperator)
        {
            StringBuilder stringBuilder = new StringBuilder(); // Combine the strings list into a string

            for (int i = 0; i < strings.Length - 1; i++)
            {
                stringBuilder.Append(strings[i]);
                stringBuilder.Append(seperator);
            }
            stringBuilder.Append(strings[strings.Length - 1]); // Get final string

            string strReturn = stringBuilder.ToString(); // Get rid of the final separator character
            strReturn = strReturn.TrimEnd(strReturn[strReturn.Length - 1]);

            return strReturn;
        }


        /// <summary>
        /// Create a string that represents the protocol's subsections
        /// </summary>
        /// <returns>A string represent the subsection for the protocol</returns>
        private List<string> CreateSectionStrings()
        {
            //string returnString;  // Create a string to represent the protocol's subsections
            List<string> returnList = new List<string>(); // Create a list of items to save

            returnList.Add(this.ChildSections.Count.ToString());
            foreach (Section section in this.ChildSections)
            {
                List<string> subsectionList = section.CreateSectionString();
                for (int i = 0; i < subsectionList.Count; i++)
                {
                    returnList.Add(subsectionList[i]);
                }

                //returnString += section.CreateSectionString ( seperator );
            }

            return returnList;
        }


        /// <summary>
        /// Deep copy the list of subsections
        /// </summary>
        /// <returns>The deep copied list of subsections</returns>
        private List<Section> CopyChildSections()
        {
            List<Section> copiedSectionList; // The list of values being copied
            int sectionCount;   // The count of subsections

            sectionCount = this.ChildSections.Count;

            if (sectionCount > 0)
            {
                copiedSectionList = new List<Section>(this.ChildSections.Count);
                foreach (Section section in this.ChildSections)
                {
                    Section copiedSection; // Create a new instance of the section being copied
                    copiedSection = new Section(section);

                    copiedSectionList.Add(copiedSection);
                }
            }
            else
            {
                copiedSectionList = new List<Section>(0);
            }

            return copiedSectionList;
        }
    }
}
