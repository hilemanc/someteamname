﻿// ----------------------------------------------------------------------------
// File Name: IOExport.cs
// Project Name: Systematic Reivew Manger
// ----------------------------------------------------------------------------
// Creator's Name: Anthony Pitman
// Creation Date: December 02, 2018
// ----------------------------------------------------------------------------
// Purpose: Handle exporting various files in format such as PDF, Text, or Word
// ----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using SystematicManager.Models.Entities;

namespace SystematicManager.Exports
{
	/// <summary>
	/// Export handling class contains static and private methods primarily
	/// </summary>
	public class IOExport
	{
		private const string WORD_EXTENSION = ".docx";
		private const string DEFAULT_DIRECTORY = "~/"; // Specify the application's current directory
		private const char SEPERATOR = '|';


		/// <summary>
		/// Takes a protocol and exports that protocol as a Microsoft Word Document
		/// </summary>
		/// <param name="protocol">The protocol that is being exported as a Word Document</param>
		public static void ExportAsAWordDocument ( Protocol protocol )
		{
			string fileName; // Get a file name and location to export a file
			ExportAsWordDocument export; // Export an inputted protocol as a Word document

			fileName = GenerateSaveString ( protocol.Title, WORD_EXTENSION );
			export = new ExportAsWordDocument ( fileName, protocol );
		}


		/// <summary>
		/// Create a string for the location and name of the file to be exported.
		/// </summary>
		/// <param name="fileName">The name of the file that is being exported</param>
		/// <param name="fileExtension">The extension, if any to add to the end of a file</param>
		/// <returns>The generated save location for a file</returns>
		private static string GenerateSaveString ( string fileName, string fileExtension )
		{
			string saveLocation = ""; // Create a path to save a value to.

			saveLocation = DEFAULT_DIRECTORY;
			if ( fileName != null )
			{
				saveLocation += fileName;
			}
			else
			{
				saveLocation += Guid.NewGuid ( );
			}


			saveLocation += fileExtension;

			return saveLocation;
		}
	}
}
