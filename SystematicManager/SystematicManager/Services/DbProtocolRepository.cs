﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SystematicManager.Data;
using SystematicManager.Models.Entities;

namespace SystematicManager.Services
{
    public class DbProtocolRepository : IProtocolRepository
    {

        private ApplicationDbContext _db;

        /// <summary>
        /// Injecting the Dbcontext into the repository
        /// </summary>
        /// <param name="db">The db context to be injected</param>
        public DbProtocolRepository(ApplicationDbContext db)
        {
            _db = db;
        }

        /// <summary>
        /// Creating the protocol in the database
        /// </summary>
        /// <param name="protocol"></param>
        /// <returns></returns>
        public Protocol Create(Protocol protocol)
        {
            _db.Protocols.Add(protocol);
            _db.SaveChanges();
            return protocol;
        }

        public Section CreateSection(int id, Section section)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public Section DeleteSection(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Reading the protocol with the given id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns the protocol with sections included</returns>
        public Protocol Read(int id)
        {
            // section depth of 4
            return _db.Protocols.Include(p => p.ChildSections).ThenInclude(c => c.ChildSections).ThenInclude(h => h.ChildSections).ThenInclude(i => i.ChildSections).FirstOrDefault(p => p.Id == id);
        }

        public Section ReadSection(int id)
        {
            throw new NotImplementedException();
        }

        public void Update(int id, Protocol protocol)
        {
            throw new NotImplementedException();
        }

        public Section UpdateSection(int id)
        {
            throw new NotImplementedException();
        }
    }
}
