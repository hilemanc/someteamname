﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SystematicManager.Models.Entities;

namespace SystematicManager.Services
{
    public interface IProtocolRepository
    {
        Protocol Create(Protocol protocol);
        Protocol Read(int id);
        void Delete(int id);
        void Update(int id, Protocol protocol);

        Section CreateSection(int id, Section section);
        Section ReadSection(int id);
        Section DeleteSection(int id);
        Section UpdateSection(int id);
    }
}
