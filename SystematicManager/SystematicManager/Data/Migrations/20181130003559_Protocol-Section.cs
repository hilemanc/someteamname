﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SystematicManager.Data.Migrations
{
    public partial class ProtocolSection : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ProtocolId",
                table: "Sections",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Sections_ProtocolId",
                table: "Sections",
                column: "ProtocolId");

            migrationBuilder.AddForeignKey(
                name: "FK_Sections_Protocols_ProtocolId",
                table: "Sections",
                column: "ProtocolId",
                principalTable: "Protocols",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Sections_Protocols_ProtocolId",
                table: "Sections");

            migrationBuilder.DropIndex(
                name: "IX_Sections_ProtocolId",
                table: "Sections");

            migrationBuilder.DropColumn(
                name: "ProtocolId",
                table: "Sections");
        }
    }
}
