﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SystematicManager.Models.Entities;

namespace SystematicManager.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<Project> Projects { get; set; }
        public DbSet<Protocol> Protocols { get; set; }
        public DbSet<Reference> References { get; set; }
        public DbSet<SysUser> SysUsers { get; set; }
        public DbSet<Project_User> Projects_Users { get; set; }

        public DbSet<Section> Sections { get; set; }

    }
}
