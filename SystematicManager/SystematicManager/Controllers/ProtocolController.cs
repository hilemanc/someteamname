﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SystematicManager.Models.Entities;
using SystematicManager.Services;

namespace SystematicManager.Controllers
{
    public class ProtocolController : Controller
    {
        private IProtocolRepository _repo;

        public ProtocolController(IProtocolRepository repo)
        {
            _repo = repo;
        }
        public IActionResult Index()
        {
            Protocol openProtocol = _repo.Read(1); // hardcoded to read the first protocol for now
            ViewBag.proto = openProtocol;
            return View();
        }

        /// <summary>
        /// Method to create protocol
        /// </summary>
        /// <returns>Returns a view to initially create a Protocol</returns>
        public IActionResult Create()
        {
            return View();
        }

        /// <summary>
        /// Create post method
        /// </summary>
        /// <param name="protocol">The protocol the user created to pass to the database</param>
        /// <returns>returns the view </returns>
        [HttpPost]
        public IActionResult Create(Protocol protocol)
        {
      
            if(ModelState.IsValid)
            {
                _repo.Create(protocol);
                return RedirectToAction("Details");
            }
            return View(protocol);
        }

    }
}