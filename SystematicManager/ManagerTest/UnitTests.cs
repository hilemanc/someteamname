using System;
using System.Collections.Generic;
using SystematicManager.Models.Entities;
using Xunit;

namespace ManagerTest
{
    public class UnitTests
    {

        [Fact]
        public void Protocol_ChildSection_Test()
        {
            //ARRANGE
            Protocol proto = new Protocol("Test_Protocol");
            Section section = new Section("Test_Section");
            section.Heading = "Heading";
            proto.AddChildSection(section);

            //ACT
            Section result = proto.GetSectionByHeading("Heading");

            //ASSERT
            Assert.Equal(section, result);
        }

        [Fact]
        public void Protocol_CreateProtocolString_Test()
        {
            //ARRANGE
            Protocol proto = new Protocol("Test_Protocol");
            List<string> expectedResult = new List<string>
            {
                "Test_Protocol", "", "", "John Doe, Jane Doe, Santa Claus", "0"
            };

            //ACT
            List<string> result = proto.CreateProtocolString();

            //ASSERT
            Assert.Equal(expectedResult, result);

        }

        [Fact]
        public void Protocol_ToString_Test()
        {
            //ARRANGE
            Protocol proto = new Protocol("Test_Protocol");
            string expectedResult = "Protocol: Title = Test_Protocol\n" +
                "\tAuthors = John Doe, Jane Doe, Santa Claus\n";

            //ACT
            string result = proto.ToString();

            //ASSERT
            Assert.Equal(expectedResult, result);
        }

        [Fact]
        public void Protocol_ToChildrenList_Test()
        {
            //ARRANGE
            Protocol proto = new Protocol("Test_Protocol");
            Section section1 = new Section("Test_Section1");
            Section section2 = new Section("Test_Section2");
            List<Section> expectedResult = new List<Section>(50);

            foreach (Section section in proto.ChildSections)
            {
                expectedResult.Add(section);
            }
            //ACT
            List<Section> result = proto.ToChildrenList();

            //ASSERT
            Assert.Equal(expectedResult, result);
        }

        [Fact]
        public void Section_ChildSection_Test()
        {
            //ARRANGE
            Section topSection = new Section("TopSection");
            Section childSection = new Section("ChildSection");
            List<Section> expectedResult = new List<Section>
            {
                childSection
            };

            //ACT
            topSection.AddChildSection(childSection);
            List<Section> result = topSection.GetChildSections();

            //ASSERT
            Assert.Equal(expectedResult, result);
        }

        [Fact]
        public void Section_ToString_Test()
        {
            //ARRANGE
            Section section = new Section("Test_Section");
            string expectedResult = "Section: Heading = Sample Heading\n" +
            "\tText = This is some sample text.\n" + 
            "\tId = Test_Section\n";

            //ACT
            string result = section.ToString();

            //ASSERT
            Assert.Equal(expectedResult, result);
        }

        [Fact]
        public void Section_CalcDepth_Test()
        {
            //ARRANGE
            Section topSection = new Section("ParentSection");
            Section middleSection = new Section("ParentSection_Child");
            Section bottomSection = new Section("ParentSection_Child_Child");
            topSection.AddChildSection(middleSection);
            middleSection.AddChildSection(bottomSection);
            int expectedResult = 3;

            //ACT
            int result = bottomSection.CalcDepth();

            //ASSERT
            Assert.Equal(expectedResult, result);
        }

        [Fact]
        public void Section_AddToList_Test()
        {
            //ARRANGE
            Section section1 = new Section("Section1");
            Section section2 = new Section("Section2");
            section1.AddChildSection(section2);

            List<Section> expectedResult = new List<Section>
            {
                section1, section2
            };
            List<Section> result = new List<Section>();

            //ACT
            section1.AddToList(ref result);

            //ASSERT
            Assert.Equal(expectedResult, result);
        }

        [Fact]
        public void Section_CreateSectionString_Test()
        {
            //ARRANGE
            Section section = new Section("Test_Section");
            List<string> expectedResult = new List<string>
            {
                "Test_Section", "Sample Heading", "This is some sample text.", "0"
            };

            //ACT
            List<string> result = section.CreateSectionString();

            //ASSERT
            Assert.Equal(expectedResult, result);
        }

        [Fact]
        public void Section_CountOfChildSections_Test()
        {
            //ARRANGE
            Section topSection = new Section("Top_Section");
            Section middleSection = new Section("Middle_Section");
            Section bottomSection = new Section("Bottom_Section");
            middleSection.AddChildSection(bottomSection);
            topSection.AddChildSection(middleSection);
            int expectedResult = 2;

            //ACT
            int result = topSection.CountOfChildSections() + 1;

            //ASSERT
            Assert.Equal(expectedResult, result);
        }
    }
}
