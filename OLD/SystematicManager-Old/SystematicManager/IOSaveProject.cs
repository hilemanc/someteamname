﻿// ---------------------------------------------------------------
// File Name: IOSaveProject.cs
// Project Name: Systematic Review Manager
// ---------------------------------------------------------------
// Creator's Name: Anthony Pitman
// Creation Date: October 21, 2018
// ---------------------------------------------------------------
// Purpose: Save a project as well as protocol and author list so
//			that they can be opened later. Functions as a save,
//			not save as.
// ---------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;

namespace SystematicManager
{
	/// <summary>
	/// Saves the inputed project to the drive in a preconfigured location
	/// </summary>
	class IOSaveProject : IO
	{
		/// <summary>
		/// Constructor that calls the base's constructor to set the project
		/// </summary>
		/// <param name="project">The project that is being set</param>
		public IOSaveProject ( Project project ) : base ( project )
		{
			this.Protocol = this.CurrentProject.Protocol;
		}


		/// <summary>
		/// Inherited method to call that starts the save process
		/// </summary>
		public override void RunTask ( )
		{
			string fileName; // The name and path of the file to save to

			fileName = this.CurrentProject.Filepath;
			Save ( fileName );
		}


		/// <summary>
		/// Save the project's information to the system drive in the specified location
		/// </summary>
		/// <param name="filename">The location of where to save the file</param>
		private void Save ( string filename )
		{
			string strSave; // Hold the string that is going to be exported

			try
			{
				strSave = GenerateSaveProjectString ( );
				File.WriteAllText ( filename, strSave, Encoding.Unicode ); // Actually write the values to the file
			}
			#region HandleExceptions
			catch ( ArgumentException )
			{
				MessageBox.Show ( "The file name cannot contain invalid characters, try to save with a valid name.", "Invalid Name", MessageBoxButton.OK, MessageBoxImage.Error );
			}
			catch ( PathTooLongException )
			{
				MessageBox.Show ( "The path to the file or the file name is too long.", "Name Too Long", MessageBoxButton.OK, MessageBoxImage.Error );
			}
			catch ( DirectoryNotFoundException )
			{
				MessageBox.Show ( "The path to the file is invalid.", "Invalid Path", MessageBoxButton.OK, MessageBoxImage.Error );
			}
			catch ( IOException )
			{
				MessageBox.Show ( "An error occured while trying to save the file. Make sure that you have write permission in the location.", "Error Saving", MessageBoxButton.OK, MessageBoxImage.Error );
			}
			catch ( UnauthorizedAccessException )
			{
				MessageBox.Show ( "The file could not be exported due to a permission error. Make sure that you have write access to the location and try again.", "Permission Error", MessageBoxButton.OK, MessageBoxImage.Error );
			}
			#endregion

			MessageBox.Show ( "The Project has been saved.", "Project Saved", MessageBoxButton.OK, MessageBoxImage.Information );
		}

		/// <summary>
		/// Build the string that is being written to the file
		/// </summary>
		/// <returns>The string to be written to a file in the form "Title|LeadAuthor|Authors|DateStart|DateEnd|PubMedMeSHRecords|Protocol" doesn't end with a '|'</returns>
		private string GenerateSaveProjectString ( )
		{
			Project project;            // Get a copy of the project to save
			List<string> outputList;    // Used to contain the strings to be written to a file

			project = this.CurrentProject;
			outputList = new List<string> ( );

			outputList.Add ( this.CurrentProject.Title );
			outputList.Add ( project.LeadAuthor.CreateSaveString ( ) );

			// Output author list
			outputList.Add ( this.CurrentProject.Authors.Count.ToString ( ) ); // Count of upcoming authors
			if ( this.CurrentProject.Authors.Count > 0 )
			{
				foreach ( Author author in this.CurrentProject.Authors )
				{
					outputList.Add ( author.CreateSaveString ( ) );
				}
			}

			outputList.Add ( this.CurrentProject.DateStart.ToString ( ) );
			outputList.Add ( this.CurrentProject.DateEnd.ToString ( ) );

			/********************************************************************************************************************************
			*  Currently, I can't copy pubmed class because it isn't set up in a way to allow me to save them.
			*  I need either a format save string or access to the properties.
			******************************************************************************************************************************/

			List<string> protocolStringList = this.CurrentProject.Protocol.CreateProtocolString ( );
			for ( int i = 0; i < protocolStringList.Count; i++ )
			{
				outputList.Add ( protocolStringList [ i ] );
			}

			return CreateOutputString ( outputList.ToArray ( ) );
		}


		/// <summary>
		/// Create the string that will be written to the file
		/// </summary>
		/// <param name="strings">The strings that are going to be written to a file</param>
		/// <returns>A string formated for writing to a file</returns>
		private string CreateOutputString ( string [ ] strings )
		{
			StringBuilder stringBuilder = new StringBuilder ( ); // Create a string to write to a file

			// Write each line to the string to ouput except the last for separation concerns
			for ( int i = 0; i < strings.Length - 1; i++ )
			{
				stringBuilder.Append ( strings [ i ] );
				stringBuilder.Append ( SEPERATOR );
			}
			stringBuilder.Append ( strings [ strings.Length - 1 ] );

			return stringBuilder.ToString ( );
		}
	}
}
