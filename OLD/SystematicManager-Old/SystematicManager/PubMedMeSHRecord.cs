﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystematicManager
{
    class PubMedMeSHRecord
    {
        public List<string> MajorDescriptors { get; private set; }  // Holds all Descriptors marked as Major Topics
        public List<string> MajorQualifiers { get; private set; }   // Holds all Qualifiers marked as Major Topics
        public List<string> MinorDescriptors { get; private set; }  // Holds all Descriptors marked as Minor Topics 
        public List<string> MinorQualifiers { get; private set; }   // Holds all Qualifiers marked as Minor Topics
        public bool IsComplete { get; private set; }                // True if MeSh Record found and populated

        /// <summary>
        /// Default Constructor
        /// </summary>
        public PubMedMeSHRecord()
        {
            IsComplete = false;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="meshFrame">A List representation of a parsed XML EFetch file from PubMed.</param>
        public PubMedMeSHRecord(List<string> meshFrame)
        {
            // Initialize all lists
            MajorDescriptors = new List<string>(20);
            MajorQualifiers = new List<string>(20);
            MinorDescriptors = new List<string>(20);
            MinorQualifiers = new List<string>(20);

            /*---   Process MeSH frame line by line. Add string to relevant list
             *      depending on xml paramaters */
            for (int i = 0; i < meshFrame.Count(); i++)
            {
                if (meshFrame[i].StartsWith("DescriptorName"))
                {
                    if (meshFrame[i].Contains("MajorTopicYN=\"N\""))
                        MinorDescriptors.Add(meshFrame[i + 1].Replace(",", ""));
                    else if (meshFrame[i].Contains("MajorTopicYN=\"Y\""))
                        MajorDescriptors.Add(meshFrame[i + 1].Replace(",", ""));
                    i++;
                }
                else if (meshFrame[i].StartsWith("QualifierName"))
                {
                    if (meshFrame[i].Contains("MajorTopicYN=\"N\""))
                        MinorQualifiers.Add(meshFrame[i + 1].Replace(",", ""));
                    else if (meshFrame[i].Contains("MajorTopicYN=\"Y\""))
                        MajorQualifiers.Add(meshFrame[i + 1].Replace(",", ""));
                    i++;
                }
            }

            IsComplete = true;
        }

		/// <summary>
		/// Parameterized constructor designed to read in information from a file
		/// </summary>
		/// <param name="majorDescriptors">Information for major descriptor list</param>
		/// <param name="majorQualifiers">Information for major qualifier list</param>
		/// <param name="minorDescriptors">Information for minor descriptor list</param>
		/// <param name="minorQualifiers">Information for minor qualifier list</param>
		public PubMedMeSHRecord ( string [ ] majorDescriptors, string [ ] majorQualifiers, string [ ] minorDescriptors, string [ ] minorQualifiers )
		{
			this.MajorDescriptors.AddRange ( majorDescriptors );
			this.MajorQualifiers.AddRange ( majorQualifiers );
			this.MinorDescriptors.AddRange ( minorDescriptors );
			this.MinorQualifiers.AddRange ( minorQualifiers );
			this.IsComplete = true;
		}

        /// <summary>
        /// Formats the MeSH Record for the article into a printable string.
        /// </summary>
        /// <returns>A formatted string representing this ArticleMeSHRecord.</returns>
        public override string ToString()
        {
            string returnStr = String.Empty;

            if (IsComplete)
            {
                returnStr += "Major Descriptors: ";
                foreach (string str in MajorDescriptors)
                    returnStr += str + " | ";
                returnStr = returnStr.TrimEnd(" |".ToCharArray());
                returnStr += "\nMinor Descriptors: ";
                foreach (string str in MinorDescriptors)
                    returnStr += str + " | ";
                returnStr = returnStr.TrimEnd(" |".ToCharArray());
                returnStr += "\n\nMajor Qualifiers: ";
                foreach (string str in MajorQualifiers)
                    returnStr += str + " | ";
                returnStr = returnStr.TrimEnd(" |".ToCharArray());
                returnStr += "\nMinor Qualifiers: ";
                foreach (string str in MinorQualifiers)
                    returnStr += str + " | ";
                returnStr = returnStr.TrimEnd(" |".ToCharArray());
                returnStr += "\n";
            }
            else
                returnStr = "No MeSH Record found in PubMed record.";

            return returnStr;
        }

		/// <summary>
		/// Create a string representation of the PubMeSHRecord object
		/// </summary>
		/// <returns>A string representation of the PubMeSHRecord object</returns>
		public List<string> GenerateSaveString ( )
		{
			List<string> returnList;	// Create a list of items to save
			int counts = 0;				// Get the counts of values in the lists

			counts = this.MajorDescriptors.Count;
			returnList = new List<string> ( counts );

			// Add the Major Descriptors
			returnList.Add ( counts.ToString ( ) );
			for ( int i = 0; i < counts; i++ )
			{
				returnList.Add ( this.MajorDescriptors [ i ] );
			}

			// Add the Major Quantifiers
			counts = this.MajorQualifiers.Count;
			for ( int i = 0; i < counts; i++ )
			{
				returnList.Add ( this.MajorQualifiers [ i ] );
			}

			// Add the Minor Descriptors
			counts = this.MinorDescriptors.Count;
			for ( int i = 0; i < counts; i++ )
			{
				returnList.Add ( this.MinorDescriptors [ i ] );
			}

			// Add the Minor Qualifiers
			counts = this.MinorQualifiers.Count;
			for ( int i = 0; i < counts; i++ )
			{
				returnList.Add ( this.MinorQualifiers [ i ] );
			}

			return returnList;
		}
    }
}
