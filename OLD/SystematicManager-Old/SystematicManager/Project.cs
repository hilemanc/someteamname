﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystematicManager
{
	class Project
	{
		public Protocol Protocol { get; set; }
		public string Title { get; set; }
		public Author ProjectManager { get; set; }
		public DateTime DateStart { get; set; }
		public DateTime DateEnd { get; set; }
		public String Filepath { get; set; }
		public List<Author> Authors { get; set; }
		private Author Lead_Author;      // backing private field for public property

		public Author LeadAuthor
		{
			get { return Lead_Author; }
			set
			{
				Lead_Author = value;
				Protocol.Authors = Lead_Author.Name; // this will be more complex as we work on User integration
			}
		}


		public Project ( string title )
		{
			Protocol = new Protocol ( title );
			Title = title;
			Authors = new List<Author> ( );
			LeadAuthor = new Author ( "Lead Author" ); // Add a default name
		}

		/// <summary>
		/// Copy constructor that makes a deep copy of the author list
		/// </summary>
		/// <param name="projIn">The project that is being copied to a new project</param>
		public Project ( Project projIn )
		{
			this.Protocol = new Protocol ( projIn.Protocol );
			this.Title = projIn.Title;
			this.LeadAuthor = new Author ( projIn.LeadAuthor );
			this.Authors = new List<Author> ( 0 );

			foreach ( Author author in projIn.Authors )
			{
				this.Authors.Add ( new Author ( author ) );
			}
		}

		public void NewProtocol ( )
		{
			Protocol = new Protocol ( Title );
		}


	}
}
