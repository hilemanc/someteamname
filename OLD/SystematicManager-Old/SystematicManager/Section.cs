﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystematicManager
{
	/// <summary>
	/// Represents a Section with text and heading, it may have child Sections of its own.
	/// </summary>
	public class Section
	{
		public string Id { get; private set; }
		public string Text { get; set; }
		public string Heading { get; set; }
		public List<Section> ChildSections { get; set; }

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="id"></param>
		public Section ( string id )
		{
			this.Id = id;
			ChildSections = new List<Section> ( 0 );

			// Dummy data to follow
			this.Text = "This is some sample text.";
			this.Heading = "Sample Heading";
		}

		/// <summary>
		/// Parameterized constructor that takes fills id, heading and text and initializes the childsections
		/// </summary>
		/// <param name="id">The section id that is being set</param>
		/// <param name="heading">The section header that is being set</param>
		/// <param name="text">The section text that is being set</param>
		public Section ( string id, string heading, string text )
		{
			this.Id = id;
			this.Heading = heading;
			this.Text = text;
			this.ChildSections = new List<Section> ( 0 );
		}

		/// <summary>
		/// Copy constructor that deep copies the fields in the section
		/// </summary>
		/// <param name="section">The section that is being deep copied.</param>
		public Section ( Section section )
		{
			this.Id = section.Id;
			this.Heading = section.Heading;
			this.Text = section.Text;
			this.ChildSections = section.CopySectionList ( );
		}

		public Section AddChildSection ( )
		{
			// Starting sequence of string ID is always parent id + _
			string childId = this.Id + "_";

			// Possibility that middle Section deleted, simply doing .Count + 1 isn't good enough.
			// If no sections exist, last of id is 0
			if ( ChildSections.Count == 0 )
				childId += "0";
			// Otherwise, last of id is newest child + 1
			else
			{
				char temp = ChildSections.Last ( ).Id.Last ( );
				childId += ( Char.GetNumericValue ( temp ) + 1 );
			}

			// Now create the child and attach to this parent's list
			Section sec = new Section ( childId );
			ChildSections.Add ( sec );
			return sec;
		}

		public Section AddChildSection ( string heading, string text )
		{
            if (heading == null || text == null)
                throw new ArgumentNullException();
			// Starting sequence of string ID is always parent id + _
			string childId = this.Id + "_";

			// Possibility that middle Section deleted, simply doing .Count + 1 isn't good enough.
			// If no sections exist, last of id is 0
			if ( ChildSections.Count == 0 )
				childId += "0";
			// Otherwise, last of id is newest child + 1
			else
			{
				char temp = ChildSections.Last ( ).Id.Last ( );
				childId += ( Char.GetNumericValue ( temp ) + 1 );
			}

			// Now create the child and attach to this parent's list
			Section sec = new Section ( childId );
			sec.Heading = heading;
			sec.Text = text;
			ChildSections.Add ( sec );
			return sec;
		}

		/// <summary>
		/// Add a child section to the section list
		/// </summary>
		/// <param name="section">The section being added to the child section list</param>
		/// <returns>The section that was added.</returns>
		public Section AddChildSection ( Section section )
		{
			this.ChildSections.Add ( section );
			return section;
		}

		/// <summary>
		/// Finds the Section with the requested ID recursively
		/// </summary>
		/// <param name="id">String ID of the Section</param>
		/// <returns>Section instance</returns>
		public Section GetSection ( string id )
		{
			if ( id == this.Id )
				return this;

			foreach ( Section sec in ChildSections )
			{
				int length = sec.Id.Length;
				string trunc = id.Substring ( 0, length );
				if ( trunc == sec.Id )
					return ( sec.GetSection ( id ) );
			}

			throw new Exception ( "Error: Section not found" );
		}

        /// <summary>
        /// Finds the Section with the requested heading recursively
        /// </summary>
        /// <param name="heading">String heading of the Section</param>
        /// <returns>Section instance</returns>
        public Section GetSectionByHeading(string heading)
        {
            if (Heading == heading)
                return this;

            foreach (Section sec in ChildSections)
            {
                if (sec.Heading == heading)
                    return sec;
                else
                {
                    Section temp = sec.GetSectionByHeading(heading);
                    if (temp != null)
                        return temp;
                }
            }
            return null;
        }


        /// <summary>
        /// Returns formatted string of the Section information
        /// </summary>
        /// <returns>formatted string</returns>
        public override string ToString ( )
		{
			// Add this Section's data
			string output = "";
			output += "Section: Heading = " + this.Heading + "\n";
			output += "\tText = " + this.Text + "\n";
			output += "\tId = " + this.Id + "\n";

			//Recursively call using child Sections
			foreach ( Section sec in ChildSections )
				output += sec.ToString ( );

			return output;
		}

		/// <summary>
		/// Calculates the depth of a child section. 
		///     Note that the count starts at zero, meaning the child directly
		///     to the parent will have depth zero. 
		/// </summary>
		/// <returns>Depth of this section as integer</returns>
		public int CalcDepth ( )
		{
			int depth = 0;
			foreach ( char c in this.Id )
				if ( c == '_' )
					depth += 1;
			return depth;
		}

		/// <summary>
		/// Adds this section and all child sections to a passed list
		/// </summary>
		/// <param name="list"></param>
		public void AddToList ( ref List<Section> list )
		{
			list.Add ( this );
			foreach ( Section sec in ChildSections )
				sec.AddToList ( ref list );
		}

		/// <summary>
		/// Create a string to represent the section in a saveable format
		/// </summary>
		/// <returns>A string that represents a section</returns>
		public List<string> CreateSectionString ( )
		{
			List<string> stringsToOuput;    // Get a list of the properties to write to the file
			int sectionCount;               // Determine how many subsections are under the current section
			List<Section> sectionList;      // Contain a listing of the subsections in a protocol

			stringsToOuput = new List<string> ( );
			sectionList = this.ChildSections;
			sectionCount = sectionList.Count;

			// Copy the fields to the string: ID, text, Heading
			stringsToOuput.Add ( this.Id );
			stringsToOuput.Add ( this.Heading );
			stringsToOuput.Add ( this.Text );

			// Get the same information from any subfields in the current field
			stringsToOuput.Add ( sectionCount.ToString ( ) );
			if ( sectionCount > 0 )
			{
				foreach ( Section section in sectionList )
				{
					List<string> subsectionList = section.CreateSectionString ( );
					for ( int i = 0; i < subsectionList.Count; i++ )
					{
						stringsToOuput.Add ( subsectionList [ i ] );
					}
				}

			}
			return stringsToOuput;// MakeString ( stringsToOuput.ToArray ( ), seperator );
		}


		/// <summary>
		/// Create a string from a given set of strings
		/// </summary>
		/// <param name="strings">The listing of strings to create a string from</param>
		/// <param name="seperator">The seperator to separate the strings of</param>
		/// <returns>A seperator delimited string of the inputted string list</returns>
		private string MakeString ( string [ ] strings, char seperator )
		{
			StringBuilder stringBuilder = new StringBuilder ( ); // Combine the strings in the list into a string

			for ( int i = 0; i < strings.Length; i++ )
			{
				stringBuilder.Append ( strings [ i ] );
				stringBuilder.Append ( seperator );
			}
			//stringBuilder.Append ( strings [ strings.Length - 1 ] ); // Get the final string

			return stringBuilder.ToString ( );
		}


		/// <summary>
		/// Deep copy the list of subsection
		/// </summary>
		/// <returns>A deep copied list of subsections</returns>
		private List<Section> CopySectionList ( )
		{
			List<Section> copiedChildSections; // Get a list to return to the caller
			int countOfChildSections;          // Get the count of the number of items in the list

			countOfChildSections = this.ChildSections.Count;

			if ( countOfChildSections > 0 )
			{
				copiedChildSections = new List<Section> ( countOfChildSections );

				for ( int i = 0; i < countOfChildSections; i++ )
				{
					Section copiedSections;    // Get a new copy of the section being copied
					copiedSections = new Section ( this.ChildSections [ i ] );
					copiedChildSections.Add ( copiedSections );
				}
			}
			else
			{
				copiedChildSections = new List<Section> ( 0 );
			}

			return copiedChildSections;
		}


		/// <summary>
		/// Get the list of child sections
		/// </summary>
		/// <returns>The list of child sections</returns>
		public List<Section> GetChildSections ( )
		{
			return this.ChildSections;
		}


		/// <summary>
		/// Returns the number of child sections that a section has
		/// </summary>
		/// <returns>The number of children sections</returns>
		public int CountOfChildSections ( )
		{
			return this.ChildSections.Count;
		}
	}
}
