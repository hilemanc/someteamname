﻿// ---------------------------------------------------------------
// File Name: IOOpenProject.cs
// Project Name: Systematic Review Manager
// ---------------------------------------------------------------
// Creator's Name: Anthony Pitman
// Creation Date: October 21, 2018
// ---------------------------------------------------------------
// Purpose: Open a project and load the information into the project
// ---------------------------------------------------------------

using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using Microsoft.Win32;

namespace SystematicManager
{
	/// <summary>
	/// Extended IO class to handle importing from a sysreview project
	/// </summary>
	class IOOpenProject : IO
	{
		/// <summary>
		/// Constructor that takes a project and calls the IO class constructor to set it
		/// </summary>
		/// <param name="project">The project that is being set</param>
		public IOOpenProject ( Project project ) : base ( project )
		{
			this.Protocol = this.CurrentProject.Protocol;
		}


		/// <summary>
		/// Open a project. To actually get the project use the project property
		/// </summary>
		public override void RunTask ( )
		{
			OpenFileDialog openFileDialog; // Open a file so that it can be imported into the program

			openFileDialog = new OpenFileDialog ( )
			{ // Set defaults for the open dialog
				CheckFileExists = true,     // Is the file actually there
				CheckPathExists = true,     // Can we actually get to the file
				DefaultExt = DEFAULT_PROJECTEXT, // Use the project's default extention
				Filter = "Systematic Review Project|*." + DEFAULT_PROJECTEXT + "|Text Files|*.txt|All Files|*.*",
				InitialDirectory = Path.GetFullPath ( Environment.CurrentDirectory + DEFAULT_DIRECTORY ),
				Multiselect = false,        // The user can't import multiple files at the same time
				Title = "Select a project to import"
			};

			Nullable<bool> dialogResults = openFileDialog.ShowDialog ( );

			if ( dialogResults == false )
			{
                throw new IOException("User Cancelled Selection");
			}
			else
			{
				Open ( openFileDialog.FileName );
			}
		}


		/// <summary>
		/// Get the text from the user selected Project file
		/// </summary>
		/// <param name="filename">The name and location of the file to open</param>
		private void Open ( string filename )
		{
			string openedFile; // Contains the text read into the program

			try
			{
				openedFile = File.ReadAllText ( filename, Encoding.Unicode );
				LoadProject ( openedFile );
				this.CurrentProject.Filepath = filename;
			}
			#region IOExceptions
			catch ( ArgumentException )
			{
				MessageBox.Show ( "The file name cannot contain invalid characters, please try to open a valid file.", "Invalid Name", MessageBoxButton.OK, MessageBoxImage.Error );
			}
			catch ( PathTooLongException )
			{
				MessageBox.Show ( "The path to the file or the file name is too long to open", "Name Too Long", MessageBoxButton.OK, MessageBoxImage.Error );
			}
			catch ( DirectoryNotFoundException )
			{
				MessageBox.Show ( "The path to the file is invalid.", "Invalid Path", MessageBoxButton.OK, MessageBoxImage.Error );
			}
			catch ( IOException )
			{
				MessageBox.Show ( "An error occurred while trying to open. Make sure that you have permission to open to the location.", "Error Opening", MessageBoxButton.OK, MessageBoxImage.Error );
			}
			catch ( UnauthorizedAccessException )
			{
				MessageBox.Show ( "The file could not be opened due to permisson errors. Make sure that you have read access to the file and try to open it again.", "Permission Error", MessageBoxButton.OK, MessageBoxImage.Error );
			}
			catch ( NotSupportedException )
			{
				MessageBox.Show ( "The path is invalid.", "Invalid Path", MessageBoxButton.OK, MessageBoxImage.Error );
			}
			#endregion
		}


		/// <summary>
		/// Fill in the Project and protocol
		/// </summary>
		/// <param name="input">The string representing the project</param>
		private void LoadProject ( string input )
		{
			string [ ] inputArray = input.Split ( '|' ); // Split the input based on the pipes
			Author authorIn;            // Divide up the author text 
			int countOfPositions = 0;   // Used to move through the input text
			int authorCount = 0;        // Count the number of additional authors
			int sectionCount = 0;       // Count the number of sections in the protocol
			DateTime dateIn;            // Read in date times

			this.CurrentProject = new Project ( inputArray [ countOfPositions++ ] );
			authorIn = RecreateAuthor ( inputArray [ countOfPositions++ ] );
			this.CurrentProject.LeadAuthor = authorIn;

			Int32.TryParse ( inputArray [ countOfPositions++ ], out authorCount ); // Number of upcoming authors
			if ( authorCount > 0 )
			{
				for ( int i = 0; i < authorCount; i++ )
				{
					authorIn = RecreateAuthor ( inputArray [ countOfPositions++ ] );
					this.CurrentProject.Authors.Add ( authorIn );
				}
			}

			DateTime.TryParse ( inputArray [ countOfPositions++ ], out dateIn );
			this.CurrentProject.DateStart = dateIn;
			DateTime.TryParse ( inputArray [ countOfPositions++ ], out dateIn );
			this.CurrentProject.DateEnd = dateIn;

			/**********************************************************************************************************************************
				* Opening Pubmed records goes here but is a stub since I currently can't write them
			***********************************************************************************************************/

			this.CurrentProject.Protocol = new Protocol ( inputArray [ countOfPositions++ ] );
			this.CurrentProject.Protocol.InclusionCriteria = inputArray [ countOfPositions++ ];
			this.CurrentProject.Protocol.ExclusionCriteria = inputArray [ countOfPositions++ ];
			this.CurrentProject.Protocol.Authors = inputArray [ countOfPositions++ ];


			// Read in the sections
			Int32.TryParse ( inputArray [ countOfPositions++ ], out sectionCount ); // Get the number of sections
			Section section; // Create a new section for each section that is being read into the program
			for ( int i = 0; i < sectionCount; i++ )
			{
				section = new Section ( inputArray [ countOfPositions++ ] );
				section.Heading = inputArray [ countOfPositions++ ];
				section.Text = inputArray [ countOfPositions++ ];
				Int32.TryParse ( inputArray [ countOfPositions ], out int subsectionCount );

				if ( subsectionCount > 0 )
				{
					RecreateSubsection ( ref countOfPositions, ref section, inputArray );
				}
				else
				{
					countOfPositions++; // Burn the extra 0
				}

				this.CurrentProject.Protocol.AddChildSection ( section ); // Add the completed section to the protocol
			}
		}


		/// <summary>
		/// Create any needed subsection for the current section. Designed as a recursive method
		/// </summary>
		/// <param name="currentPosition">A reference to the current position in the input array</param>
		/// <param name="sectionToChange">A reference to the section that is getting a subsection</param>
		/// <param name="inputArray">The lisitng of inputs</param>
		/// <returns>A section if one exists or otherwise null</returns>
		private void RecreateSubsection ( ref int currentPosition, ref Section sectionToChange, string [ ] inputArray )
		{
			Section subsection = null;     // Create a new section to contain the current information
			int subsectionCount = 0;       // Count the number of subsections a section has


			Int32.TryParse ( inputArray [ currentPosition++ ], out subsectionCount ); // Get the number of child sections
			for ( int i = 0; i < subsectionCount; i++ )
			{
				subsection = new Section ( inputArray [ currentPosition++ ] );
				subsection.Heading = inputArray [ currentPosition++ ];
				subsection.Text = inputArray [ currentPosition++ ];
				RecreateSubsection ( ref currentPosition, ref subsection, inputArray ); // Add any additional subsections

				sectionToChange.AddChildSection ( subsection );
			}
		}


		/// <summary>
		/// Create an author from an inputed text string in proper format
		/// </summary>
		/// <param name="input">Author information in a formated string</param>
		/// <returns>An object of author</returns>
		private Author RecreateAuthor ( string input )
		{
			Author author; // Used to create an author from text with a comma separator
			string [ ] fields; // Separate the input into the fields

			fields = Regex.Split ( input, ", " );

			author = new Author ( fields [ 0 ], fields [ 2 ], fields [ 1 ] );

			return author;
		}
	}
}
