﻿// ---------------------------------------------------------------
// File Name: IOExportDocument.cs
// Project Name: Systematic Review Manager
// ---------------------------------------------------------------
// Creator's Name: Anthony Pitman
// Creation Date: October 14, 2018
// ---------------------------------------------------------------
// Purpose: Inherited class that class an overriden method that
//			begins the creation of a Microsoft Word document based
//			on a given Project.
// ---------------------------------------------------------------

using System;
using System.IO;
using System.Windows;
using Microsoft.Win32;

namespace SystematicManager
{
	/// <summary>
	/// Extended IO class to handle exporting to a Microsoft Word Document
	/// </summary>
	class IOExportDocument : IO
	{
		/// <summary>
		/// Constructor that calls the base's constructor to set the project
		/// </summary>
		/// <param name="project">The project the contains the desired protocol</param>
		public IOExportDocument ( Project project ) : base ( project )
		{
			this.Protocol = this.CurrentProject.Protocol;
		}

		/// <summary>
		/// Export the protocol to a word document
		/// </summary>
		/// <param name="projectWithProtoclToExport">The protocol that is being exported</param>
		/// <returns>Null, nothing, just there for conformity</returns>
		public override void RunTask ( )
		{
			SaveFileDialog saveFileDialog; // Ask the user where to put the Word document

			saveFileDialog = new SaveFileDialog
			{
				AddExtension = true,    // Automatically add the word extension if the user doesn't
				CheckPathExists = true, // Make sure the path is valid
				DefaultExt = "docx",    // Make a word document the file extention
				FileName = this.Protocol.Title,     // Suggest a title for the export
				Filter = "Word Documents|*.docx;*.doc;*.dotx", // Set the filter for the type of files
				InitialDirectory = Path.GetFullPath ( Environment.CurrentDirectory + DEFAULT_DIRECTORY ),
				Title = "Select a location or a file to overwrite." // Set the title of the save dialog box
			};

			Nullable<bool> results = saveFileDialog.ShowDialog ( );

			if ( results == true )
			{
				if ( saveFileDialog.FileName != "" && saveFileDialog.FileName != null )
				{
					try
					{
						ExportWordDocument exportWordDocument = new ExportWordDocument ( saveFileDialog.FileName, this.CurrentProject );

						MessageBox.Show ( "The Protocol was successfully exported to a Microsoft Word document.", "Export Successful", MessageBoxButton.OK, MessageBoxImage.Information );
					}
					catch ( Exception ex ) // Probably many exceptions, but they were difficult to find in the documentation
					{
						MessageBox.Show ( "The Export to a Word document failed with the message:\n" + ex.Message, "Export Failed", MessageBoxButton.OK, MessageBoxImage.Error );
					}
				}
				else
				{
					MessageBox.Show ( "Error a file must be selected, try again.", "Export Cancelled", MessageBoxButton.OK, MessageBoxImage.Information );
				}
			}
			else
			{
				MessageBox.Show ( "Export to word document was cancelled by the user", "Export Cancelled", MessageBoxButton.OK, MessageBoxImage.Information );
			}
		}
	}
}
