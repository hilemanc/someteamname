﻿// ---------------------------------------------------------------
// File Name: IOExportProtocl.cs
// Project Name: Systematic Review Manager
// ---------------------------------------------------------------
// Creator's Name: Anthony Pitman
// Creation Date: October 14, 2018
// ---------------------------------------------------------------
// Purpose: Inherited class that allows a protocol to be exported
//			as a text document so that it can be opened easily.
// ---------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.Win32;
using System.Windows;

namespace SystematicManager
{
	/// <summary>
	/// Extended to handle exporting to a text file with the extension .sysreview
	/// </summary>
	class IOExportProtocol : IO
	{
		/// <summary>
		/// Constructor that calls the base's constructor to set the project
		/// </summary>
		/// <param name="project">The project that is being exported</param>
		public IOExportProtocol ( Project project ) : base ( project )
		{
			this.Protocol = this.CurrentProject.Protocol;
		}

		/// <summary>
		/// Returns the protocol that was given to it. Should be ignored
		/// </summary>
		/// <param name="projectIn">The protocol to export</param>
		/// <returns>The protocol that was given to the Export</returns>
		public override void RunTask ( )
		{
			SaveFileDialog saveFileDialog; // Create a save file dialog to display to the user

			saveFileDialog = new SaveFileDialog
			{ // Set the default values for a save file dialog box
				AddExtension = true,            // Automatically add the extention if it is not added
				CheckFileExists = false,         // Ask the user if they want to replace an existing file
				CheckPathExists = true,         // Make sure that it is a real path
				DefaultExt = DEFAULT_EXTENSION, // Use the default extention
				Filter = "Systematic Reviews|*." + DEFAULT_EXTENSION + "|TextFiles|*.txt",
				InitialDirectory = Path.GetFullPath ( Environment.CurrentDirectory + DEFAULT_DIRECTORY ),
				FileName = this.CurrentProject.Title, // Suggest a default save location
				OverwritePrompt = true,
				Title = "Select an Export Location"
			};

			Nullable<bool> dialogResults = saveFileDialog.ShowDialog ( );

			if ( dialogResults == true )
			{
				if ( saveFileDialog.FileName != "" && saveFileDialog.FileName != null )
				{
					Export ( saveFileDialog.FileName );
				}
				else
				{
					string errorMessage = "The file could not be exported due to an error. Make sure that you have write access in the location and try again.";
					MessageBox.Show ( errorMessage, "Export Error", MessageBoxButton.OK, MessageBoxImage.Error );
				}
			}
			else
			{
				MessageBox.Show ( "Exporting the protocol was canceled by the user.", "Exporting Canceled", MessageBoxButton.OK, MessageBoxImage.Information );
			}
		}


		/// <summary>
		/// Perform the work of exporting the values of the protocol to a text file
		/// </summary>
		/// <param name="filename">The path to the file where the protocol information is to be placed</param>
		private void Export ( string filename )
		{
			try
			{
				string strExport; // Hold the string that is going to be exported

				List<string> protocolStringList = this.Protocol.CreateProtocolString ( );
				strExport = MakeOutputString ( protocolStringList.ToArray ( ) );
				File.WriteAllText ( filename, strExport, Encoding.Unicode ); // Actually write the values to the file
			}
			#region HandleExceptions
			catch ( ArgumentException )
			{
				string errorMessage = "The file name cannot contain invalid characters please try to export again.";
				MessageBox.Show ( errorMessage, "Invalid Name", MessageBoxButton.OK, MessageBoxImage.Error );
			}
			catch ( PathTooLongException )
			{
				string errorMessage = "The path to the file or the file name is too long to export";
				MessageBox.Show ( errorMessage, "Name Too Long", MessageBoxButton.OK, MessageBoxImage.Error );
			}
			catch ( DirectoryNotFoundException )
			{
				string errorMessage = "The path to the file is invalid.";
				MessageBox.Show ( errorMessage, "Invalid Path", MessageBoxButton.OK, MessageBoxImage.Error );
			}
			catch ( IOException )
			{
				string errorMessage = "An error occurred while trying to export. Make sure that you have permission to write to the location.";
				MessageBox.Show ( errorMessage, "Error Exporting", MessageBoxButton.OK, MessageBoxImage.Error );
			}
			catch ( UnauthorizedAccessException )
			{
				string errorMessage = "The file could not be exported due to permisson errors. Make sure that you have write access to the location and try to export again.";
				MessageBox.Show ( errorMessage, "Permission Error", MessageBoxButton.OK, MessageBoxImage.Error );
			}
			#endregion

			MessageBox.Show ( "The protocol was successfully exported.", "Export Successful", MessageBoxButton.OK, MessageBoxImage.Information );
		}


		private string MakeOutputString ( string [ ] strings )
		{
			StringBuilder stringBuilder;

			stringBuilder = new StringBuilder ( );
			for ( int i = 0; i < strings.Length - 1; i++ )
			{
				stringBuilder.Append ( strings [ i ] );
				stringBuilder.Append ( SEPERATOR );
			}
			stringBuilder.Append ( strings [ strings.Length - 1 ] );

			return stringBuilder.ToString ( );
		}

	}
}
