﻿// ---------------------------------------------------------------
// File Name: IO.cs
// Project Name: Systematic Review Manager
// ---------------------------------------------------------------
// Creator's Name: Anthony Pitman
// Creation Date: October 14, 2018
// ---------------------------------------------------------------
// Purpose: Abstract class the serves the purpose of giving
//			derived classes a standard method that they implement
//			to give IO functionality.
// ---------------------------------------------------------------

namespace SystematicManager
{
	/// <summary>
	/// Used to create child classes with the same strings and a similar method
	/// </summary>
	class IO
	{
		protected const string DEFAULT_DIRECTORY = "..\\..\\data";
		protected const string DEFAULT_EXTENSION = "sysreview";
		protected const string DEFAULT_PROJECTEXT = "srproj";
		protected const char SEPERATOR = '|';

		/// <summary>
		/// 
		/// </summary>
		public Protocol Protocol { get; set; }

		/// <summary>
		/// Used to manage interactions with the project
		/// </summary>
		public Project CurrentProject { get; set; }

		/// <summary>
		/// Parameterized construtor that takes the project that is being opened or saved
		/// </summary>
		/// <param name="project">The project that is being saved</param>
		public IO (Project project)
		{
			this.CurrentProject = project;
			this.Protocol = project.Protocol;
		}

		/// <summary>
		/// Perform the task based on the child class
		/// </summary>
		/// <param name="protocol">The protocol that currently exists. In the case of Import in may be just a new protocol.</param>
		/// <returns>In the case of Export returns the inputed protocol. In the case of Import, it returns the new protocol.</returns>
		public virtual void RunTask (  )
		{
			
		}
		
	}
}
