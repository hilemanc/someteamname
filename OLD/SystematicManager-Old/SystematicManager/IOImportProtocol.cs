﻿// ---------------------------------------------------------------
// File Name: IOImportProtocol.cs
// Project Name: Systematic Review Manager
// ---------------------------------------------------------------
// Creator's Name: Anthony Pitman
// Creation Date: October 14, 2018
// ---------------------------------------------------------------
// Purpose: Inherited class that calls a overriden method to
//			import a text document into the program.
// ---------------------------------------------------------------

using System;
using System.IO;
using System.Text;
using System.Windows;
using Microsoft.Win32;

namespace SystematicManager
{
	/// <summary>
	/// Extended IO class to handle importing from a .sysreview text fileImport
	/// </summary>
	class IOImportProtocol : IO
	{
		/// <summary>
		/// Constructor that takes a project and calls the IO class constructor to set it
		/// </summary>
		/// <param name="project">The project that is being set</param>
		public IOImportProtocol ( Project project ) : base ( project )
		{
			this.Protocol = this.CurrentProject.Protocol;
		}

		/// <summary>
		/// Import a protocol into the program
		/// </summary>
		public override void RunTask ( )
		{
			OpenFileDialog openFileDialog; // Open a file so that it can be imported into the program

			openFileDialog = new OpenFileDialog ( )
			{ // Set defaults for the open dialog
				CheckFileExists = true,         // Is the file really there?
				CheckPathExists = true,         // Make sure the path is really there
				DefaultExt = DEFAULT_EXTENSION, // Use the default extention
				Filter = "Systematic Reviews|*.sysreview|TextFiles|*.txt|All Files|*.*",
				InitialDirectory = Path.GetFullPath ( Environment.CurrentDirectory + DEFAULT_DIRECTORY ),
				Multiselect = false,            // Don't select multiple files
				Title = "Select a file to Import."
			};

			Nullable<bool> dialogResults = openFileDialog.ShowDialog ( );

			if ( dialogResults == false )
			{
				MessageBox.Show ( "Importing a file was canceled.", "Import Canceled", MessageBoxButton.OK, MessageBoxImage.Information );
			}
			else
			{
				if ( openFileDialog.FileName != "" && openFileDialog.FileName != null )
				{
					Import ( openFileDialog.FileName );
				}
				else
				{
					MessageBox.Show ( "File could not be imported. Please check the location and try again." );
				}
			}
		}


		/// <summary>
		/// Read the contents of the file into a Protocol
		/// </summary>
		/// <param name="filename">The name of the file that gets opened</param>
		private void Import ( string filename )
		{
			string openedFile; // Contains the text read in from the file

			try
			{
				openedFile = File.ReadAllText ( filename, Encoding.Unicode );
				BuildProtocol ( openedFile );
			}
			#region IOExceptions
			catch ( ArgumentException )
			{
				string errorMessage = "The file name cannot contain invalid characters, please try to open a valid file.";
				MessageBox.Show ( errorMessage, "Invalid Name", MessageBoxButton.OK, MessageBoxImage.Error );
			}
			catch ( PathTooLongException )
			{
				string errorMessage = "The path to the file or the file name is too long to open";
				MessageBox.Show ( errorMessage, "Name Too Long", MessageBoxButton.OK, MessageBoxImage.Error );
			}
			catch ( DirectoryNotFoundException )
			{
				string errorMessage = "The path to the file is invalid.";
				MessageBox.Show ( errorMessage, "Invalid Path", MessageBoxButton.OK, MessageBoxImage.Error );
			}
			catch ( IOException )
			{
				string errorMessage = "An error occurred while trying to open. Make sure that you have permission to open to the location.";
				MessageBox.Show ( errorMessage, "Error Opening", MessageBoxButton.OK, MessageBoxImage.Error );
			}
			catch ( UnauthorizedAccessException )
			{
				string errorMessage = "The file could not be opened due to permisson errors. Make sure that you have read access to the file and try to open it again.";
				MessageBox.Show ( errorMessage, "Permission Error", MessageBoxButton.OK, MessageBoxImage.Error );
			}
			catch ( NotSupportedException )
			{
				string errorMessage = "The path is invalid.";
				MessageBox.Show ( errorMessage, "Invalid Path", MessageBoxButton.OK, MessageBoxImage.Error );
			}
			#endregion
		}

		/// <summary>
		/// Fill the parts of the protocol so that it can be returned to the caller
		/// </summary>
		/// <param name="input">The string of protocol inputs</param>
		private void BuildProtocol ( string input )
		{
			string [ ] inputArray = input.Split ( '|' ); // Split the input based on the lines
			int numberOfSections = 0;   // Check how many sections are in the list there are
			int countOfPositions = 0;   // Used to work through the text file

			this.Protocol = new Protocol ( "" );

			this.Protocol.Title = inputArray [ countOfPositions++ ];
			this.Protocol.InclusionCriteria = inputArray [ countOfPositions++ ];
			this.Protocol.ExclusionCriteria = inputArray [ countOfPositions++ ];
			this.Protocol.Authors = inputArray [ countOfPositions++ ];

			Int32.TryParse ( inputArray [ countOfPositions++ ], out numberOfSections ); // Get section count
			Section section; // Create a new section for each section that is being read into the program
			for ( int i = 0; i < numberOfSections; i++ )
			{
				section = new Section ( inputArray [ countOfPositions++ ] );
				section.Heading = inputArray [ countOfPositions++ ];
				section.Text = inputArray [ countOfPositions++ ];
				Int32.TryParse ( inputArray [ countOfPositions ], out int subsectionCount );
				if ( subsectionCount > 0 )
				{
					RecreateSection ( ref countOfPositions, ref section, inputArray );
				}
				else
				{
					countOfPositions++; // Burn the extra 0
				}

				this.Protocol.AddChildSection ( section ); // Add the completed section to the protocol.
			}
		}


		/// <summary>
		/// Creates the needed subsections for the current section. Designed as a recursive method
		/// </summary>
		/// <param name="currentPosition">A reference to the current position in the input array</param>
		/// <param name="inputArray">The listing of inputs</param>
		/// <returns>A section if one exists or otherwise null</returns>
		private void RecreateSection ( ref int currentPosition, ref Section sectionToChange, string [ ] inputArray )
		{
			Section section = null;     // Create a new section to contain the current information
			int subsectionCount = 0;    // Count the number of subsections a section has

			Int32.TryParse ( inputArray [ currentPosition++ ], out subsectionCount ); // Get number of child sections
			for ( int i = 0; i < subsectionCount; i++ )
			{
				section = new Section ( inputArray [ currentPosition++ ] );
				section.Heading = inputArray [ currentPosition++ ];
				section.Text = inputArray [ currentPosition++ ];
				RecreateSection ( ref currentPosition, ref section, inputArray );

				sectionToChange.AddChildSection ( section );
			}
		}


	}
}
