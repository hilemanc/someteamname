﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystematicManager
{
    class Author
    {
        public string Name { get; set; }   // Author name
        public string Email { get; set; }  // Author email address
        public string Title { get; set; }  // Author title (ex: Dr., Mr., Ms., Mrs.)

        /// <summary>
        /// Parameterized constructor for Author, assumes all params are used.
        /// </summary>
        /// <param name="name">Author's name</param>
        /// <param name="title">Author's title</param>
        /// <param name="email">Author's email</param>
        public Author(string name, string title, string email)
        {
            // Set all the parameter fields
            Name = name;
            Title = title;
            Email = email;
        }

        /// <summary>
        /// Parameterized constructor for Author with name and email
        /// </summary>
        /// <param name="name">Author's name</param>
        /// <param name="email">Author's email</param>
        public Author(string name, string email)
        {
            // Set name and email fields
            Name = name;
            Email = email;
			Title = "";
        }

        /// <summary>
        /// Parameterized constructor for Author with only name
        /// </summary>
        /// <param name="name">Author's name</param>
        public Author(string name)
        {
            // Set name
            Name = name;
			Email = "";
			Title = "";
        }

		/// <summary>
		/// Copy constructor for Author
		/// </summary>
		/// <param name="author">The author object that is being copied</param>
		public Author (Author author)
		{
			Name = author.Name;
			Email = author.Email;
			Title = author.Title;
		}

		/// <summary>
		/// Output a comma and space separated list in order of Name, Email, Title
		/// </summary>
		/// <returns>A string of the Author in the form Name, Email, Title</returns>
		public string CreateSaveString ( )
		{
			string strOut; // Output a string that represents the object for saving
			const string SEPERATOR = ", ";

			strOut = this.Name;
			strOut += SEPERATOR;
			strOut += this.Email;
			strOut += SEPERATOR;
			strOut += this.Title;

			return strOut;
		}
    }
}
