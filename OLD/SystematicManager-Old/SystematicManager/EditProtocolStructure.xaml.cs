﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SystematicManager
{
    /// <summary>
    /// Interaction logic for EditProtocolStructure.xaml
    /// </summary>
    public partial class EditProtocolStructure : Window
    {
        public Protocol proto;              // protocol used for this window
        Point lastMouseDown;                // for tracking mousedown events
        TreeViewItem draggedItem = null;    // for tracking the item being dragged
        Boolean isChild = false;            // flag for isSibling/isChild item addition
        DateTime dragEntry;                 // for tracking timestamp of a drag's entry into a TreeViewItem's space

        /// <summary>
        /// Default Constructor
        /// </summary>
        /// <param name="proto">Protocol that will be edited.</param>
        public EditProtocolStructure(Protocol proto)
        {
            InitializeComponent();
            this.proto = proto;
            PopulateWindow();
        }

        #region Load In Protocol

        /// <summary>
        /// Populates the window. Initializes the first level of Sections and 
        /// passes to recursive loop.
        /// </summary>
        private void PopulateWindow()
        {
            // First, create a root for tree
            foreach (Section sec in proto.GetChildSections())
            {
                TreeViewItem sectionRoot = AddItem(sec.Heading);
                foreach (Section subsec in sec.ChildSections)
                {
                    ProcessSubSections(sectionRoot, subsec);
                }
            }
        }
    
        /// <summary>
        /// Adds all subsections to the relevant TreeView
        /// in a recursive manner.
        /// </summary>
        /// <param name="parent">Parent to take section as child</param>
        /// <param name="section">Child to be added to parent</param>
        private void ProcessSubSections(TreeViewItem parent, Section section)
        {
            TreeViewItem entry = new TreeViewItem();
            entry.Header = section.Heading;
            entry.Style = (Style)FindResource("TreeViewItemStyle");
            foreach (Section sec in section.ChildSections)
                ProcessSubSections(entry, sec);
            parent.Items.Add(entry);
        }
        
        /// <summary>
        /// Creates a TreeViewItem and adds it to the root level 
        /// of the Tree. 
        /// </summary>
        /// <param name="sectionHeading">Heading of the Section</param>
        /// <returns>The created TreeViewItem.</returns>
        private TreeViewItem AddItem(string sectionHeading)
        {
            TreeViewItem entry = new TreeViewItem();
            entry.Header = sectionHeading;
            entry.Style = (Style)FindResource("TreeViewItemStyle");
            ProtocolView.Items.Add(entry);
            return entry;
        }
        #endregion

        #region Button and TextBox Handlers

        /// <summary>
        /// Handles a close event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCloseWindow_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        /// <summary>
        /// Allows the upper 20px of the window to be a drag control for the window.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void recDragBar_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }

        /// <summary>
        /// Handles adding a new section to the Protocol Structure
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAddSection_Click(object sender, RoutedEventArgs e)
        {
            var t = new TreeViewItem();
            t.Header = txtSectionName.Text;
            t.Style = (Style)FindResource("TreeViewItemStyle");
            ProtocolView.Items.Add(t);

            txtSectionName.Text = "Insert Section Name...";
        }

        /// <summary>
        /// Clears the textbox on gaining focus.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSectionName_GotFocus(object sender, RoutedEventArgs e)
        {
            ((TextBox)sender).Clear();
        }

        /// <summary>
        /// Removes the selected section from the TreeView
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRemoveSection_Click(object sender, RoutedEventArgs e)
        {
            TreeViewItem t = (TreeViewItem)ProtocolView.SelectedItem;
            var p = t.Parent;       // I LOVE VAR
            if (p.GetType() == typeof(TreeView))
                ((TreeView)p).Items.Remove(t);
            else if (p.GetType() == typeof(TreeViewItem))
                ((TreeViewItem)p).Items.Remove(t);
        }
        #endregion

        #region Drag and Drop Logic

        /// <summary>
        /// When mouse first goes down, begins tracking position.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TreeViewItem_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                lastMouseDown = e.GetPosition(ProtocolView);
        }

        /// <summary>
        /// When the item is dropped, handle the TreeView model
        /// and check for appropriate depth.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TreeViewItem_Drop(object sender, DragEventArgs e)
        {
            e.Handled = true;
            TreeViewItem target = GetNearestContainer(e.OriginalSource as UIElement);

            if (GetDepth(target) < 4)  // Restrict to a depth of 5
            {
                if (target.Header != draggedItem.Header)
                {
                    if (!isChild)
                    {
                        var parent = target.Parent;

                        if (parent is TreeView)
                        {
                            CopyItemAsSibling((TreeView)parent, target);
                        }
                        else if (parent is TreeViewItem)
                        {
                            CopyItemAsSibling((TreeViewItem)parent, target);
                        }
                    }
                    else
                    {
                        CopyItemAsChild(target);
                    }
                }
            }
            else
                MessageBox.Show("Cannot have a Protocol depth of more than 4.", "Excessive depth.", MessageBoxButton.OK, MessageBoxImage.Asterisk);

            draggedItem.IsSelected = true;
            ResetIndicators(target);
        }
    
        /// <summary>
        /// Calculates the depth of a given node.
        /// </summary>
        /// <param name="node">Beginning node to count.</param>
        /// <returns>Returns the depth of the node.</returns>
        private int GetDepth(TreeViewItem node)
        {
            int i = 1;
            TreeViewItem t = node;
            while (true)
            {
                if (t.Parent is TreeView)
                    break;
                else
                {
                    t = (TreeViewItem)t.Parent;
                    i++;
                }
            }
            return i;
        }

        /// <summary>
        /// Traverses up branch from node and resets
        /// all indicators.
        /// </summary>
        /// <param name="node"></param>
        private void ResetIndicators(TreeViewItem node)
        {
            TreeViewItem t = node;
            while (true)
            {
                t.Background = Brushes.Transparent;
                t.BorderBrush = Brushes.Transparent;
                if (t.Parent is TreeView)
                    break;
                else
                    t = (TreeViewItem)t.Parent;
            }
        }

        /// <summary>
        /// Copy dragged item into a TreeViewItem as a child.
        /// </summary>
        /// <param name="parent">Parent Item.</param>
        private void CopyItemAsChild(TreeViewItem parent)
        {
            TreeViewItem item = draggedItem;

            var dragParent = draggedItem.Parent;
            if (dragParent is TreeView)
                ((TreeView)dragParent).Items.Remove(draggedItem);
            else if (dragParent is TreeViewItem)
                ((TreeViewItem)dragParent).Items.Remove(draggedItem);

            parent.Items.Add(item);
        }

        /// <summary>
        /// Copy dragged item beside TreeViewItem as a sibling on root level.
        /// </summary>
        /// <param name="parent">Parent of dragged item.</param>
        /// <param name="sibling">Closest sibling of dragged item.</param>
        private void CopyItemAsSibling(TreeView parent, TreeViewItem sibling )
        {
            TreeViewItem item = draggedItem;

            var dragParent = draggedItem.Parent;
            if (dragParent is TreeView)
                ((TreeView)dragParent).Items.Remove(draggedItem);
            else if (dragParent is TreeViewItem)
                ((TreeViewItem)dragParent).Items.Remove(draggedItem);

            parent.Items.Insert(parent.Items.IndexOf(sibling) + 1, item);
        }

        /// <summary>
        /// Copy dragged item beside TreeViewItem as sibling on non-root level.
        /// </summary>
        /// <param name="parent">Parent of dragged item.</param>
        /// <param name="sibling">Closest sibling of dragged item.</param>
        private void CopyItemAsSibling(TreeViewItem parent, TreeViewItem sibling)
        {
            TreeViewItem item = draggedItem;

            var dragParent = draggedItem.Parent;
            if (dragParent is TreeView)
                ((TreeView)dragParent).Items.Remove(draggedItem);
            else if (dragParent is TreeViewItem)
                ((TreeViewItem)dragParent).Items.Remove(draggedItem);

            parent.Items.Insert(parent.Items.IndexOf(sibling) + 1, item);
            
        }

        /// <summary>
        /// Handles the trigger and animation for the dragging event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TreeViewItem_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                Point point = e.GetPosition(ProtocolView);

                if (Math.Abs(point.X - lastMouseDown.X) > SystemParameters.MinimumHorizontalDragDistance ||
                    Math.Abs(point.Y - lastMouseDown.Y) > SystemParameters.MinimumVerticalDragDistance)
                {
                    draggedItem = (TreeViewItem)ProtocolView.SelectedItem;

                    if (draggedItem != null)
                    {
                        DragDrop.DoDragDrop(ProtocolView, ProtocolView.SelectedValue, DragDropEffects.Move);
                    }
                }
            }

        }

        /// <summary>
        /// Controls the appearance of an item that is passed over by a drag event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TreeViewItem_DragOver(object sender, DragEventArgs e)
        {
            TimeSpan span = DateTime.Now.Subtract(dragEntry);

            if (draggedItem.Header != ((TreeViewItem)sender).Header)
            {
                if (span.Seconds > .8)
                {
                    ((TreeViewItem)sender).Background = Brushes.PaleVioletRed;
                    ((TreeViewItem)sender).IsExpanded = true;
                    isChild = true;
                }
                else
                    ((TreeViewItem)sender).BorderBrush = Brushes.PaleVioletRed;
            }
        }

        /// <summary>
        /// Controls the appearance of an item that has just been passed over by a drag event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TreeViewItem_DragLeave(object sender, DragEventArgs e)
        {
            dragEntry = DateTime.Now;
            ((TreeViewItem)sender).BorderBrush = Brushes.Transparent;
            ((TreeViewItem)sender).Background = Brushes.Transparent;
            isChild = false;
        }

        /// <summary>
        /// Makes a timestamp of a drag event's entry into another item.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TreeViewItem_DragEnter(object sender, DragEventArgs e)
        {
            dragEntry = DateTime.Now;
        }

        /// <summary>
        /// Finds the closest container to an element.
        /// </summary>
        /// <param name="element">The element to use for searching.</param>
        /// <returns>The closest TreeViewItem to the passed element.</returns>
        private TreeViewItem GetNearestContainer(UIElement element)
        {
            // Walk up the element tree to the nearest tree view item.
            TreeViewItem container = element as TreeViewItem;
            while ((container == null) && (element != null))
            {
                element = VisualTreeHelper.GetParent(element) as UIElement;
                container = element as TreeViewItem;
            }
            return container;
        }

        #endregion

        #region Submit Handler

        /// <summary>
        /// Creates a protocol from the TreeView. 
        /// 
        /// First, the upper level is constructed. And then each branch
        /// is recursively constructed. 
        /// 
        /// If a section with the same heading name exists in the old Protocol,
        /// the Text of that Section is copied into the new Section.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            Protocol newProto = new Protocol(proto.Title, proto.Authors);
            foreach (TreeViewItem t in ProtocolView.Items)
            {
                string tempText;
                Section temp;
                temp = proto.GetSectionByHeading(t.Header.ToString());
                if (temp == null)
                {
                    temp = newProto.AddChildSection();
                    temp.Heading = t.Header.ToString();
                    temp.Text = "";
                }
                else
                {
                    tempText = temp.Text;
                    temp = newProto.AddChildSection(t.Header.ToString(), tempText);
                }   
                foreach (TreeViewItem sub in t.Items)
                {
                    AddChildSections(sub, temp);
                }
            }
            proto = newProto;
            this.DialogResult = true;
            this.Close();
        }

        /// <summary>
        /// Recursive function for adding all sections of a branch to a
        /// Section. Also ensures that a Section with the same heading 
        /// is loaded into the new Section.
        /// </summary>
        /// <param name="t"></param>
        /// <param name="s"></param>
        private void AddChildSections(TreeViewItem t, Section s)
        {
            string tempHeading;
            string tempText;
            Section temp;
            temp = proto.GetSectionByHeading(t.Header.ToString());
            if (temp == null)
            {
                temp = s.AddChildSection();
                temp.Heading = t.Header.ToString();
                temp.Text = "";
            }
            else
            {
                tempText = temp.Text;
                temp = s.AddChildSection(t.Header.ToString(), tempText);
            }
            foreach (TreeViewItem sub in t.Items)
            {
                AddChildSections(sub, temp);
            }
        }
        #endregion
    }
}
