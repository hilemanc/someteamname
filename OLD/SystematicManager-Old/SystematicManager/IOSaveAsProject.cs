﻿// ---------------------------------------------------------------
// File Name: IOSaveAsProject.cs
// Project Name: Systematic Review Manager
// ---------------------------------------------------------------
// Creator's Name: Anthony Pitman
// Creation Date: November 05, 2018
// ---------------------------------------------------------------
// Purpose: Save a project as well as protocol and author list so
//			that they can be opened later. Functions as a save as.
// ---------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;
using Microsoft.Win32;

namespace SystematicManager
{
	/// <summary>
	/// Saves the inputted project to the drive in a location of the user's choice
	/// </summary>
	class IOSaveAsProject : IO
	{
		/// <summary>
		/// Constructor that calls the base's constructor to set the project
		/// </summary>
		/// <param name="project">The project that is being saved</param>
		public IOSaveAsProject ( Project project ) : base ( project )
		{
			this.Protocol = this.CurrentProject.Protocol;
		}


		/// <summary>
		// Inherited method to call that starts the save process
		/// </summary>
		public override void RunTask ( )
		{
			string fileName; // The name and path of the file that is being saved

			fileName = RequestSaveFile ( );
			Save ( fileName );
		}

		/// <summary>
		/// Ask the user where to save the project to and what to call the project
		/// </summary>
		/// <returns>The location and name of the file to save.</returns>
		private string RequestSaveFile ( )
		{
			string returnString = ""; // The file and path that will be returned
			SaveFileDialog saveFileDialog; // Create a save file dialog to display to the user

			saveFileDialog = new SaveFileDialog
			{ // Set the default values for the save file dialog
				AddExtension = true,			 // Automatically add the default extention if the user doesn't give on
				CheckFileExists = false,         // Ask if the user wants to replace and existing file
				CheckPathExists = true,          // Make sure the file's path is real
				DefaultExt = DEFAULT_PROJECTEXT, // Set the project's default extension as the file type
				FileName = this.CurrentProject.Title, // Suggest a default title
				Filter = "Systematic Review Projects|*." + DEFAULT_PROJECTEXT + "|TextFiles|*.txt",
				InitialDirectory = Path.GetFullPath ( Environment.CurrentDirectory + DEFAULT_DIRECTORY ),
				OverwritePrompt = true,
				Title = "Select an Save Location",
				ValidateNames = true
			};

			Nullable<bool> dialogResults = saveFileDialog.ShowDialog ( );

			if ( dialogResults == true )
			{
				returnString = saveFileDialog.FileName;
			}

			return returnString;
		}


		/// <summary>
		/// Save the project's information to the system drive in the specified location
		/// </summary>
		/// <param name="filename">The location of where to save the file</param>
		private void Save ( string filename )
		{
			string strSave; // Hold the string that is going to be exported

			try
			{
				strSave = GenerateSaveProjectString ( );
				File.WriteAllText ( filename, strSave, Encoding.Unicode ); // Actually write the values to the file
			}
			#region HandleExceptions
			catch ( ArgumentException )
			{
				MessageBox.Show ( "The file name cannot contain invalid characters, try to save with a valid name.", "Invalid Name", MessageBoxButton.OK, MessageBoxImage.Error );
			}
			catch ( PathTooLongException )
			{
				MessageBox.Show ( "The path to the file or the file name is too long.", "Name Too Long", MessageBoxButton.OK, MessageBoxImage.Error );
			}
			catch ( DirectoryNotFoundException )
			{
				MessageBox.Show ( "The path to the file is invalid.", "Invalid Path", MessageBoxButton.OK, MessageBoxImage.Error );
			}
			catch ( IOException )
			{
				MessageBox.Show ( "An error occured while trying to save the file. Make sure that you have write permission in the location.", "Error Saving", MessageBoxButton.OK, MessageBoxImage.Error );
			}
			catch ( UnauthorizedAccessException )
			{
				MessageBox.Show ( "The file could not be exported due to a permission error. Make sure that you have write access to the location and try again.", "Permission Error", MessageBoxButton.OK, MessageBoxImage.Error );
			}
			#endregion

			MessageBox.Show ( "The Project has been saved.", "Project Saved", MessageBoxButton.OK, MessageBoxImage.Information );
		}


		/// <summary>
		/// Build the string that is being written to the file
		/// </summary>
		/// <returns>The string to be written to a file in the form "Title|LeadAuthor|Authors|DateStart|DateEnd|PubMedMeSHRecords|Protocol" doesn't end with a '|'</returns>
		private string GenerateSaveProjectString ( )
		{
			Project project;            // Get a copy of the project to save
			List<string> outputList;    // Used to contain the strings to be written to a file

			project = this.CurrentProject;
			outputList = new List<string> ( );

			outputList.Add ( this.CurrentProject.Title );
			outputList.Add ( project.LeadAuthor.CreateSaveString ( ) );

			// Output author list
			outputList.Add ( this.CurrentProject.Authors.Count.ToString ( ) ); // Count of upcoming authors
			if ( this.CurrentProject.Authors.Count > 0 )
			{
				foreach ( Author author in this.CurrentProject.Authors )
				{
					outputList.Add ( author.CreateSaveString ( ) );
				}
			}

			outputList.Add ( this.CurrentProject.DateStart.ToString ( ) );
			outputList.Add ( this.CurrentProject.DateEnd.ToString ( ) );

			/********************************************************************************************************************************
			*  Currently, I can't copy pubmed class because it isn't set up in a way to allow me to save them.
			*  I need either a format save string or access to the properties.
			******************************************************************************************************************************/

			List<string> protocolStringList = this.CurrentProject.Protocol.CreateProtocolString ( );
			for ( int i = 0; i < protocolStringList.Count; i++ )
			{
				outputList.Add ( protocolStringList [ i ] );
			}

			return CreateOutputString ( outputList.ToArray ( ) );
		}


		/// <summary>
		/// Create the string that will be written to the file
		/// </summary>
		/// <param name="strings">The strings that are going to be written to a file</param>
		/// <returns>A string formated for writing to a file</returns>
		private string CreateOutputString ( string [ ] strings )
		{
			StringBuilder stringBuilder = new StringBuilder ( ); // Create a string to write to a file

			// Write each line to the string to ouput except the last for separation concerns
			for ( int i = 0; i < strings.Length - 1; i++ )
			{
				stringBuilder.Append ( strings [ i ] );
				stringBuilder.Append ( SEPERATOR );
			}
			stringBuilder.Append ( strings [ strings.Length - 1 ] );

			return stringBuilder.ToString ( );
		}
	}
}
