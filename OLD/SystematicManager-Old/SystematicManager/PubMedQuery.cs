﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Xml.Linq;

namespace SystematicManager
{
    /// <summary>
    /// Holds static methods to fetch PubMed using the eutils API. 
    /// </summary>
    class PubMedQuery
    {
        /// <summary>
        /// Handles transformation from article title to the returned
        /// XML PubMed record.
        /// </summary>
        /// <param name="articleName">The full name of the article.</param>
        /// <remarks>Assumes that the first record returned from ESearch is the article. Therefore, full article name is necessary.</remarks>
        /// <returns>A string representation of the XML PubMed record. If error with HTTP Request, the empty string is returned.</returns>
        public static string QueryByNameAuthor(string articleName, string firstAuthorName)
        {
            string pmid = String.Empty;
            string returned = String.Empty;
            string baseURL = @"https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?";
            string nameFix = articleName.Replace(' ', '+');
            string authorFix = firstAuthorName.Replace(' ', '+').Replace(',', '+');
            string options = @"db=pubmed&term=" + nameFix + "+" + authorFix;
            string url = baseURL + options;

            return Fetch(url);
        }

        /// <summary>
        /// Handles transformation from the article PMID to the 
        /// XML PubMed Record.
        /// </summary>
        /// <param name="pmid">The PubMed ID number of the article.</param>
        /// <returns>
        /// A string representation of the XML PubMed record. If error with HTTP Request 
        /// (such as invalid PMID), the empty string is returned.
        /// </returns>
        public static string FetchXMLByPMID(string pmid)
        {
            string returned = String.Empty;
            string baseURL = @"https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?";
            string options = @"db=pubmed&id=" + pmid + @"&retmode=xml";
            string url = baseURL + options;

            returned = Fetch(url);

            return returned;
        }

        /// <summary>
        /// Fetch handler for PubMed. Takes passed url and returns response.
        /// </summary>
        /// <param name="url">URL in the expected eutils format.</param>
        /// <returns>        
        /// A string representation of the XML PubMed record. If error with HTTP Request, 
        /// the empty string is returned.
        /// </returns>
        private static string Fetch(string url)
        {
            string retString = String.Empty;

            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    retString = reader.ReadToEnd();
                }
            } catch (WebException ex)
            {
                return String.Empty;
            }

            // If length is less the 200, the returned file is most likely an error message.
            if (retString.Length < 200)
                retString = String.Empty;

            return retString;
        }

        /// <summary>
        /// Scans through xml file (of ESearch response type) to locate the <i>first</i> PMID.
        /// </summary>
        /// <param name="output"></param>
        /// <returns></returns>
        private static string GetPMID(string output)
        {
            string[] outputFix = output.Split('<');
            string idVal = String.Empty;
            foreach (string row in outputFix)
            {
                if (row.StartsWith("Id>"))
                {
                    idVal = row.TrimStart(new char[]{ '>', 'I', 'd'});
                    break;
                }
            }

            return idVal;
        }

        /// <summary>
        /// Public facing method to handle full translation from PMID to 
        /// the MeSH Record for an Article.
        /// </summary>
        /// <param name="pmid">The PMID of the article.</param>
        /// <returns>An <see cref="PubMedMeSHRecord"/> containing all MeSH data from the article.</returns>
        public static PubMedMeSHRecord GetMeSHTermsByPMID(string pmid)
        { 
            return ExtractMeShTerms(FetchXMLByPMID(pmid));
        }

        /// <summary>
        /// Public facing method to handle full translation from article name to
        /// the MeSH Record for an Article.
        /// </summary>
        /// <param name="articleName">The full name of the article.</param>
        /// <returns>An <see cref="PubMedMeSHRecord"/> containing all MeSH data from the article.</returns>
        public static PubMedMeSHRecord GetMeSHTermsByName(string articleName, string author)
        {
            return ExtractMeShTerms(FetchXMLByPMID(GetPMID(QueryByNameAuthor(articleName,author))));
        }

        /// <summary>
        /// Public facing method to handle translation from article name to
        /// text return of abstract and publication information.
        /// </summary>
        /// <param name="articleName">The full name of the article.</param>
        /// <returns>A string represenation of the text returned by the query.</returns>
        public static string GetAbstractByName(string articleName,string author)
        {
            string returned = FetchAbstractByPMID(GetPMID(QueryByNameAuthor(articleName, author)));
            if (returned == String.Empty)
            {
                returned = "No record pound for " + articleName;
                returned = returned.EndsWith(".") ? returned += "." : returned;
            }

            return returned;
        }
        
        /// <summary>
        /// Public facing method to handle translation from article id to
        /// text return of abstract and publication information.
        /// </summary>
        /// <param name="pmid">The PubMed ID of the article.</param>
        /// <returns>A string representation of the text returned by the query.</returns>
        public static string GetAbstractByPMID(string pmid)
        {
            string returned = FetchAbstractByPMID(pmid);
            if (returned == String.Empty)
                returned = "No record found for pmid " + pmid + ".";
            return returned;
        }

        /// <summary>
        /// Formats URL for retrieving an abstract with passed pmid and returns
        /// the text of the abstract and publication information.
        /// </summary>
        /// <param name="pmid">The PubMed ID of the article.</param>
        /// <returns>A string representation of the text returned by the query.</returns>
        private static string FetchAbstractByPMID(string pmid)
        {
            string returned = String.Empty;
            string baseURL = @"https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?";
            string options = @"db=pubmed&id=" + pmid + @"&retmode=text&rettype=abstract";
            string url = baseURL + options;

            returned = Fetch(url);

            return returned;
        }
        /// <summary>
        /// Parses the xmlresponse, bounds the response by framing the MeSH data,
        /// and returns an ArticleMeSHRecord containing the MeSH data for the article.
        /// </summary>
        /// <param name="xmlresponse">The XML PubMed Record returned by Fetch.</param>
        /// <returns>An <see cref="PubMedMeSHRecord"/> containing all MeSH data from the article.</returns>
        private static PubMedMeSHRecord ExtractMeShTerms(string xmlresponse)
        {
            // If an error occured in fetching the xml, the passed string will be empty. 
            if (xmlresponse == String.Empty)
                return new PubMedMeSHRecord();

            XElement parsedResponse = XElement.Parse(xmlresponse);
            string[] xml = parsedResponse.ToString(SaveOptions.DisableFormatting).Replace('<','|').Replace('>','|').Split('|');

            List<string> meshFrame = new List<string>();

            // Locates the beginning index of the MeSH frame.
            int i = 0;
            for (; i < xml.Length; i++)
            {
                if (xml[i] == "MeshHeadingList")
                {
                    i++;
                    break;
                }
            }

            PubMedMeSHRecord record;
            try
            {
                // Adds all MeSh records to the MeSH frame 
                while (!xml[i].StartsWith(@"/MeshHeadingList"))
                {
                    meshFrame.Add(xml[i]);
                    i++;
                }
                record = new PubMedMeSHRecord(meshFrame);
            }
            catch (IndexOutOfRangeException indexError)
            {
                // If index is out of range, no MeSH record is in the file. Therefore, call the default constructor.
                record = new PubMedMeSHRecord();
            }

            return record;
        }
    }
}
