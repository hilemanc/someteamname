﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;

namespace SystematicManager
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Project ActiveProject { get; set; }
        public MainWindow()
        {
            // Unit Testing
            ProtocolAddSectionUnitTest.RunTest();

            this.Hide();
            UserLogin ul = new UserLogin();
            ul.ShowDialog();
            ProjectSelectionWindow ps = new ProjectSelectionWindow();
            if(ps.ShowDialog() == true)
            {
                ActiveProject = (Project)ps.ActiveProject;
                InitializeComponent();
                this.Show();
            }
            else
            {
                this.Close();
            }
        }

        /// <summary>
        /// Controls the selection tab control resizing.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (protocol_tab.IsSelected)
            {
                protocol_tab.SetCurrentValue(HeightProperty, 28.0);
            }
            else
            {
                protocol_tab.SetCurrentValue(HeightProperty, 25.0);
            }

            if (project_tab.IsSelected)
            {
                project_tab.SetCurrentValue(HeightProperty, 28.0);
            }
            else
            {
                project_tab.SetCurrentValue(HeightProperty, 25.0);
            }

            if (useractivity_tab.IsSelected)
            {
                useractivity_tab.SetCurrentValue(HeightProperty, 28.0);
            }
            else
            {
                useractivity_tab.SetCurrentValue(HeightProperty, 25.0);
            }

        }

        /// <summary>
        /// Allows grabbing the blue bar on the window to Drag the window.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Rectangle_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }

        /// <summary>
        /// Handles Minimize Button Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MinButton_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = System.Windows.WindowState.Minimized;
        }

        /// <summary>
        /// Maximize Button Handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Known borderless window issue is resolved via margin resizing.</remarks>
        private void MaxButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.WindowState == System.Windows.WindowState.Normal)
            {
                this.WindowState = System.Windows.WindowState.Maximized;
                this.MainContainer.Margin = new Thickness(7); // Deals with borderless window maximize issue.
            }
            else if (this.WindowState == System.Windows.WindowState.Maximized)
            {
                this.WindowState = System.Windows.WindowState.Normal;
                this.MainContainer.Margin = new Thickness(0); // Resets window to normal margins.
            }
        }
        /// <summary>
        /// Close Button Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Populates all tab data on window load.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainWindow1_Loaded(object sender, RoutedEventArgs e)
        {
            ProjectTitleLabel.Text = ActiveProject.Title;
            PopulateProtocolTab();
        }

        /// <summary>
        /// Populates Protocol tab with Protocol fields.
        /// </summary>
        private void PopulateProtocolTab()
        {
            List<Section> list = ActiveProject.Protocol.ToChildrenList();

            TextBox titleHeading = new TextBox { Name = "TitleHeading", Text = "Title", Style = (Style)FindResource("Heading1") };
            ProtocolPane.Children.Add(titleHeading);
            Grid.SetRow(titleHeading, 0);
            RegisterName(titleHeading.Name, titleHeading);

            TextBox titleBox = new TextBox { Name = "Title", Text = ActiveProject.Protocol.Title, Style = (Style)FindResource("TextFormat") };
            ProtocolPane.Children.Add(titleBox);
            Grid.SetRow(titleBox, 1);
            RegisterName(titleBox.Name, titleBox);

            TextBox authorHeading = new TextBox { Name = "AuthorHeading", Text = "Author", Style = (Style)FindResource("Heading1") };
            ProtocolPane.Children.Add(authorHeading);
            Grid.SetRow(authorHeading, 2);
            RegisterName(authorHeading.Name, authorHeading);

            TextBox authorBox = new TextBox { Name = "Authors", Text = ActiveProject.Protocol.Authors, Style = (Style)FindResource("TextFormat") };
            ProtocolPane.Children.Add(authorBox);
            Grid.SetRow(authorBox, 3);
            RegisterName(authorBox.Name, authorBox);


            int rowindex = 4;
            int depth;
            string resourceName = String.Empty;
            foreach (Section sec in list)
            {
                depth = sec.CalcDepth();
                resourceName = "Heading" + (depth+1); /// resource name is Heading_ (_ = depth < 5)
                TextBox headingBox;

                headingBox = new TextBox { Name = "_" + sec.Id + "_heading", Text = sec.Heading, Style = (Style)FindResource(resourceName) };

                Grid.SetRow(headingBox, rowindex);
                ProtocolPane.Children.Add(headingBox);
                rowindex++;
                RegisterName(headingBox.Name, headingBox);

                String sectionText;

                if (String.IsNullOrEmpty(sec.Text))
                    sectionText = "No text entered for this section.";
                else
                    sectionText = sec.Text;


                TextBox textBox = new TextBox { Name = "_" + sec.Id, Text = sec.Text, Style = (Style)FindResource("TextFormat") };
                Grid.SetRow(textBox, rowindex);
                ProtocolPane.Children.Add(textBox);
                RegisterName(textBox.Name, textBox); // so we can reference it in code-behind later
                rowindex++;

            }
        }

        /// <summary>
        /// When a text box in the protocol tab is clicked twice, set box to editable.
        /// </summary>
        /// <param name="sender">Initiator of event</param>
        /// <param name="e">Event instance</param>
        private void ProtoTextBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            TextBox textBox = (TextBox)sender;
            if(textBox.Name != "Authors")
                textBox.IsReadOnly = false;
        }

        /// <summary>
        /// When a text box in the protocol tab loses focus, set it to read only.
        /// </summary>
        /// <param name="sender">Initiator of event.</param>
        /// <param name="e">Event instance.</param>
        private void ProtoTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            TextBox textBox = (TextBox)sender;
            textBox.IsReadOnly = true;
            if (textBox.Name == "Title")
            {
                if (textBox.Text != ActiveProject.Protocol.Title)
                    ProtocolSaveButton.IsEnabled = true;
            }
            else if (textBox.Name == "Authors")
            {
                if (textBox.Text != ActiveProject.Protocol.Authors)
                    ProtocolSaveButton.IsEnabled = true;
            }
            else
            {
                string sectionName = textBox.Name.Remove(0, 1);
                if (textBox.Text != ActiveProject.Protocol.GetSection(sectionName).Text)
                    ProtocolSaveButton.IsEnabled = true;
            }
        }

        /// <summary>
        /// Writes all changes made to Protocol in UI back to
        /// this Project's Protocol.
        /// </summary>
        /// <param name="sender">Initiator of event</param>
        /// <param name="e">Event instance</param>
        private void ProtocolSaveButton_Click(object sender, RoutedEventArgs e)
        {
            List<Section> list = ActiveProject.Protocol.ToChildrenList();

            TextBox tb = new TextBox();
            for (int i=0; i < list.Count; i++)
            {
                tb = (TextBox)FindName("_" + list[i].Id);
                if (tb != null) // FindName will return null if failed to locate item; Not all headings will have text. 
                    list[i].Text = tb.Text;
                tb = (TextBox)FindName("_" + list[i].Id + "_heading");
                list[i].Heading = tb.Text;
            }

            ActiveProject.Protocol.Title = ((TextBox)FindName("Title")).Text;
            IOSaveProject io = new IOSaveProject(ActiveProject);
            io.RunTask();
            io = null; //To call GC on io object
            ProjectTitleLabel.Text = ActiveProject.Title;
            ProtocolSaveButton.IsEnabled = false;
        }

		/// <summary>
		/// Export the word document 
		/// </summary>
		/// <param name="sender">Identifies the sending event</param>
		/// <param name="e">The event that occured</param>
		private void btnExportReview_Click ( object sender, RoutedEventArgs e )
		{
			IO io; // Create a parent object to hold the IOExportDocument object

			io = new IOExportDocument ( this.ActiveProject );
			io.RunTask ( );
			io = null;
		}

        /// <summary>
        /// Launches a new window for a new project view.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnChangeProjectSidebar_Click(object sender, RoutedEventArgs e)
        {
            MainWindow window = new MainWindow();
            window.WindowStartupLocation = WindowStartupLocation.CenterOwner;
        }

        /// <summary>
        /// Launches 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditStructureButton_Click(object sender, RoutedEventArgs e)
        {
            EditProtocolStructure editWindow = new EditProtocolStructure(ActiveProject.Protocol);
            if (editWindow.ShowDialog() == true)
            {
                ActiveProject.Protocol = editWindow.proto;
                foreach (TextBox t in ProtocolPane.Children)
                {
                    UnregisterName(t.Name);
                }
                ProtocolPane.Children.Clear();
                PopulateProtocolTab();
                ProtocolSaveButton.IsEnabled = true;
            }
        }
    }
}
