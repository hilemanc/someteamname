﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SystematicManager
{
    /// <summary>
    /// Interaction logic for ProjectSelectionWindow.xaml
    /// </summary>
    public partial class ProjectSelectionWindow : Window
    {
        public object ActiveProject { get; set; } // Store a created project

        /// <summary>
        /// Default Constructor
        /// </summary>
        public ProjectSelectionWindow()
        {
            InitializeComponent();
        }


        /// <summary>
        /// Closes the window when the X button is clicked.
        /// </summary>
        /// <param name="sender">Initiating item</param>
        /// <param name="e">Triggered Event</param>
        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            Close();
        }

        /// <summary>
        /// Returns to the OpenGrid with an animation. 
        /// </summary>
        /// <param name="sender">Initiating item</param>
        /// <param name="e">Triggered event</param>
        private void CloseButton2_Click(object sender, RoutedEventArgs e)
        {
            ThicknessAnimation anim = new ThicknessAnimation();

            anim.From = NewProjectGrid.Margin;
            anim.To = new Thickness(250, 0, 0, 0);
            anim.SpeedRatio = 2;

            NewProjectGrid.BeginAnimation(Button.MarginProperty, anim);
            lblProjectTitle.BeginAnimation(Label.MarginProperty, anim);
            btnCreateAndOpenProject.BeginAnimation(Button.MarginProperty, anim);
            CloseButton2.BeginAnimation(Button.MarginProperty, anim);
            NewProjectGrid.Visibility = Visibility.Hidden;
            OpenGrid.Visibility = Visibility.Visible;

            txtProjectTitle.Text = String.Empty;
        }

        /// <summary>
        /// Create New Project button is clicked on the OpenGrid, triggering
        /// a quick animation on the window.
        /// </summary>
        /// <param name="sender">Initiating item</param>
        /// <param name="e">Triggered event</param>
        private void btnCreateNewProject_Click(object sender, RoutedEventArgs e)
        {
            ThicknessAnimation anim = new ThicknessAnimation();

            anim.From = NewProjectGrid.Margin;
            anim.To = new Thickness(0);
            anim.SpeedRatio = 3;

            OpenGrid.Visibility = Visibility.Hidden;
            NewProjectGrid.Visibility = Visibility.Visible;
            NewProjectGrid.BeginAnimation(Button.MarginProperty, anim);
            lblProjectTitle.BeginAnimation(Label.MarginProperty, anim);
            btnCreateAndOpenProject.BeginAnimation(Button.MarginProperty, anim);

            txtProjectTitle.Focus();
        }

        /// <summary>
        /// If user Creates New Project, instantiates Project and closes window.
        /// </summary>
        /// <param name="sender">Initiating item</param>
        /// <param name="e">Triggered event</param>
        private void btnCreateAndOpenProject_Click(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrEmpty(txtProjectTitle.Text))
                MessageBox.Show("Project must have a title.", "Cannot Create Project", MessageBoxButton.OK, MessageBoxImage.Error);
            else
            {
                ActiveProject = new Project(txtProjectTitle.Text);
                this.DialogResult = true;
                this.Close();
            }
        }

        private void btnOpenExistingProject_Click(object sender, RoutedEventArgs e)
        {
            ActiveProject = new Project("temp");
            IOOpenProject io = new IOOpenProject((Project)ActiveProject);
            try
            {
                io.RunTask();
            } catch (Exception)
            {
                io = null;
                return;
            }
            ActiveProject = io.CurrentProject;
            io = null; // To call GC on io object
            this.DialogResult = true;
            this.Close();
        }
    }
}
